<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?global $isShowMiddleAdvBottomBanner;?>
<?if($isShowMiddleAdvBottomBanner):?>
	<?$APPLICATION->IncludeComponent(
	"aspro:com.banners.next", 
	".default", 
	array(
		"IBLOCK_TYPE" => "aspro_next_content",
		"IBLOCK_ID" => "",
		"TYPE_BANNERS_IBLOCK_ID" => "",
		"SET_BANNER_TYPE_FROM_THEME" => "Y",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "ID",
		"SORT_ORDER2" => "DESC",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "URL",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"BANNER_TYPE_THEME" => "SMALL",
		"COMPONENT_TEMPLATE" => ".default",
		"BANNER_TYPE_THEME_CHILD" => "20",
		"FILTER_NAME" => "arRegionLink",
		"NEWS_COUNT2" => "4",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CATALOG" => "/catalog/"
	),
	false
);?>
<?endif;?>