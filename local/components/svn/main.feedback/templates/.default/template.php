<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="icons_elements">
    <?foreach($arResult["PLITKA"] as $key => $elPlitka):?>
        <div class="element_pl">
            <div class="img_pl">
                <a href="/test/frm1.php?id_el=<?=$key;?>" rel="nofollow" class="modalbox" ide="<?=$key;?>" frm='<?=$elPlitka["FORM_CODE"];?>'><img src="<?=$elPlitka["ICON"]["src"];?>" alt="<?=$elPlitka["NAME"];?>" /></a>
            </div>
            <?/*<div class="element_name"><a href="/test/frm1.php?id_el=<?=$key;?>" rel="nofollow" frm='<?=$elPlitka["FORM_CODE"];?>' class="modalbox"><?=$elPlitka["NAME"];?></a></div>*/?>
        </div>
    <?endforeach;?>
</div>

<div class="mfeedback icons_form" id="feedback">

</div>

<script type="text/javascript">
    $(document).ready(function(){
        //$(".modalbox").attr("href", url);
        /*url = "/test/frm1.php?id_el="+$("a.modalbox").attr('ide');

        $(".modalbox").attr("href", url);*/

        $(".modalbox").fancybox({
           'width': 520,
           'height': 500,
			'fitToView': false,
        	'autoSize': false,
             /*'transitionIn': 'elastic', // this option is for v1.3.4
            'transitionOut': 'elastic', // this option is for v1.3.4*/
			/* 'margin' : [220, 60, 250, 60],*/
            
			'type': 'iframe'

        });
       /* $(".modalbox").on("click", function(){
            url = "/test/frm1.php?id_el="+$(this).attr('ide');

            $(this).attr("href", url);
        });*/
        $("#f_contact").submit(function(){ return false; });
        $("#f_send").on("click", function(){

            // тут дальнейшие действия по обработке формы
            // закрываем окно, как правило делать это нужно после обработки данных
            $("#f_contact").fadeOut("fast", function(){
                $(this).before("<p><strong>Ваше сообщение отправлено!</strong></p>");
                setTimeout("$.fancybox.close()", 1000);
            });
        });
    });
</script>