<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

@set_time_limit(0);
define("NOT_CHECK_PERMISSIONS", true);

$lock = fopen('script_users.lock', 'w');
var_dump($lock);
if ($lock) {
    exit( 'already running' );
}

$csvFile = new CCSVData('R', true);
$csvFile->LoadFile("phones.csv");
$csvFile->SetDelimiter(';');

$arResult = array();
$records_upd_phone = 0;
$records_upd_bonus= 0;
$records_add = 0;
$print = false;
$log2file = true;
$sql_push = false; /** !!!! */
$start = microtime(true);
$end_time = 0;

while ($arRes = $csvFile->Fetch()) {

    $phone = $arRes[1];
    /*$phone = preg_replace(
        '/^(\d)(\d{3})(\d{3})(\d{4})$/',
        '+\1 (\2) \3-\4',
        (string)$phone
    );*/
    $arUserRes["PERSONAL_PHONE"] = $phone;
    $arUserRes["NAME"] = $arRes[0];
    $arUserRes["UF_SKBONUS"] = $arRes[2];
    $arUserRes["PERSONAL_CITY"] = $arRes[3];

    $arResult["CSV"][$arRes[2]] = $arUserRes;
    $arResult["CSV_PHONES"][$phone] = $arRes[2];
}

$res = Bitrix\Main\UserTable::getList(Array(
    "select" => Array("ID","NAME","PERSONAL_PHONE","UF_SKBONUS"),
    "filter" => Array("ACTIVE" => "Y"),
    "data_doubling" => false)
);

/** Update user */

while ($arRes = $res->fetch()) {
    if(empty($arRes["UF_SKBONUS"])){
        if(array_key_exists($arRes["PERSONAL_PHONE"], $arResult["CSV_PHONES"]) === true){
            $arRes["UF_SKBONUS"] = $arResult["CSV_PHONES"][$arRes["PERSONAL_PHONE"]];
            if($sql_push) {
                $fields = Array(
                    'UF_SKBONUS' => $arRes["UF_SKBONUS"]
                );
                $user = new CUser;
                $user->Update($arRes["ID"], $fields);
            }
            if($print) echo "Upd: ".$arRes["ID"]." -> ".$arResult["CSV_PHONES"][$arRes["PERSONAL_PHONE"]]."<br>";
            if($log2file) file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/import_users_log.txt", PHP_EOL . "Upd: ID -> ".$arRes["ID"]." -> ".$arResult["CSV_PHONES"][$arRes["PERSONAL_PHONE"]]." ".date("Y-m-d H:i:s"), FILE_APPEND);
            $records_upd_bonus++;
        }
    }
    if(empty($arRes["PERSONAL_PHONE"])){
        if(array_key_exists($arRes["UF_SKBONUS"], $arResult["CSV"]) === true)
        {
            if($print) echo "Upd: ".$arRes["ID"]." -> ".$arResult["CSV"][$arRes["UF_SKBONUS"]]["PERSONAL_PHONE"]."<br>";
            if($log2file) file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/import_users_log.txt", PHP_EOL . "Upd: ID -> ".$arRes["ID"]." -> ".$arResult["CSV"][$arRes["UF_SKBONUS"]]["PERSONAL_PHONE"]." ".date("Y-m-d H:i:s"), FILE_APPEND);
            if($sql_push) {
                $fields = Array(
                    'PERSONAL_PHONE' => $arResult["CSV"][$arRes["UF_SKBONUS"]]["PERSONAL_PHONE"]
                );
                $user = new CUser;
                $user->Update($arRes["ID"], $fields);
            }

            $records_upd_phone++;
        }
    }
    if(!empty($arRes["PERSONAL_PHONE"]) && !empty($arRes["UF_SKBONUS"])){
        $arResult["LIB_PHONES"][$arRes["PERSONAL_PHONE"]] = $arRes["UF_SKBONUS"];
    }
    $arResult["LIB"][$arRes["UF_SKBONUS"]] = $arRes;
}

/** Add user*/

foreach ($arResult["CSV"] as $item){
    if(array_key_exists($item["UF_SKBONUS"], $arResult["LIB"]) === false && array_key_exists($item["PERSONAL_PHONE"], $arResult["LIB_PHONES"]) === false){
        if($sql_push) {
            exit;
            $password = randString(7);
            $arFields = Array(
                "NAME" => $item["NAME"],
                "EMAIL" => $item["UF_SKBONUS"] . "@inglot.com.ru",
                "LOGIN" => $item["UF_SKBONUS"],
                "ACTIVE" => "Y",
                "GROUP_ID" => array(3, 4),
                "PASSWORD" => $password,
                "CONFIRM_PASSWORD" => $password,
                "UF_SKBONUS" => $item["UF_SKBONUS"],
                "PERSONAL_PHONE" => $item["PERSONAL_PHONE"],
                "PERSONAL_CITY" => $item["PERSONAL_CITY"]
            );

            $user = new CUser;
            $ID = $user->Add($arFields);
        }
        $records_add++;
        if (intval($ID) > 0) {
            if ($print) echo "Add: " . $ID . " -> " . $item["NAME"];
            if ($log2file) file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/import_users_log.txt", PHP_EOL . "Add: " . $ID . " -> " . $item["NAME"]." ". date("Y-m-d H:i:s"), FILE_APPEND);
        }
        else {
            if ($print) echo $user->LAST_ERROR;
            if ($log2file) file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/import_users_log.txt", PHP_EOL . "Add: " . $user->LAST_ERROR . " " . date("Y-m-d H:i:s"), FILE_APPEND);
        }
    }
}
$end_time = round(microtime(true) - $start, 2);
echo  "<h3>Всего обновлено телефонов:".$records_upd_phone."</br></h3>";
echo  "<h3>Всего обновлено номеров бонусных карт:".$records_upd_bonus."</br></h3>";
echo  "<h3>Всего добавлено новых пользователей:".$records_add."</br></h3>";
echo 'Время выполнения скрипта: '.$end_time.' сек.</br>';
echo 'Время выполнения скрипта: '.round($end_time/60, 1).' минуты.';

if($log2file) file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/import_users_log.txt",
    PHP_EOL . "Всего обновлено телефонов:".$records_upd_phone.PHP_EOL."Всего обновлено номеров бонусных карт:".$records_upd_bonus.PHP_EOL ."Всего добавлено новых пользователей:".$records_add."".PHP_EOL."Импорт завершен".PHP_EOL."Время выполнения скрипта: ".$end_time." сек.",
    FILE_APPEND);

if($print == true) p($arResult);

unlink('script_users.lock');