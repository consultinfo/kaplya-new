<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?php
    //определяем параметры товара
   //$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "SECTION_ID",);//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
    global $USER;

    if ($USER->IsAuthorized()){
        $userId=$USER->GetID();
        $rsUser = CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();

    }


    $arFilter = Array("IBLOCK_ID"=>IntVal($_REQUEST["IBLOCK_ID"]), "ID"=>$_REQUEST["ELEMENT_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false); //, $arSelect);
    $nameSection="";
    if($ob = $res->GetNextElement()){
        $arFields = $ob->GetFields();
       /*echo "<pre>";
        print_r($arFields);
        echo "</pre>";*/
        $sectionRes = CIBlockSection::GetByID($arFields["IBLOCK_SECTION_ID"]);
        if($ar_res = $sectionRes->GetNext())
            $nameSection=$ar_res['NAME'];

            $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arFields["ID"], "CATALOG_GROUP_ID" => "1",));

        if ($ar_res = $db_res->Fetch())
        {
            $price=$ar_res["PRICE"];

        }

    }

?>

<div class="popup-intro">
    <div class="pop-up-title">Купить в кредит</div>
</div>
<a class="jqmClose close"><i></i></a>
<div class="form-wr">
    <form action="https://loans-qa.tcsbank.ru/api/partners/v1/lightweight/create" method="post" id="frm-credit">
<?// var_dump($_REQUEST["ELEMENT_ID"]);?>
        <input name='shopId' value='test_online' type='hidden'/>
        <input name='showcaseId' value='test_online' type='hidden'/>
        <input name='promoCode' value='default' type='hidden'/>
        <input name='sum' value='0.00' type='hidden' id="sum">
        <input name='itemName_0' value='<?=$arFields["NAME"];?>' type='hidden' id="itemName_0"/>
        <input name='itemQuantity_0' value='1' type='hidden' id="itemQuantity_0"/>
        <input name='itemPrice_0' value='<?=$price;?>' type='hidden' id="itemPrice_0"/>
        <input name='itemCategory_0' value='<?=$nameSection;?>' type='hidden' id="itemCategory_0"/>
       <? if ($USER->IsAuthorized()):?>
        <input name='customerEmail' value='<?=$arUser["EMAIL"];?>' type='hidden'/>
        <input name='customerPhone' value='<?=$arUser["PERSONAL_PHONE"];?>' type='hidden'/>
        <?else:?>
        <input name='customerEmail' value='' type='hidden'/>
        <input name='customerPhone' value='' type='hidden'/>
        <?endif;?>
        <div class='but-r clearfix'>
            <input type='submit' value='Купи в кредит' class="btn btn-default" onClick="requestCredit('#frm-credit'); return false;"/>
        </div>







    </form>
    <div class="one_click_buy_result" id="one_click_buy_result">
        <div class="one_click_buy_result_success"><?=GetMessage('ORDER_SUCCESS')?></div>
        <div class="one_click_buy_result_fail"><?=GetMessage('ORDER_ERROR')?></div>
        <div class="one_click_buy_result_text"><?=GetMessage('ORDER_SUCCESS_TEXT')?></div>
    </div>
</div>
<script >
    $("#itemQuantity_0").val($("input[name=quantity]").val());
    $("#sum").val($("input[name=quantity]").val()*$("#itemPrice_0").val());
  /*  if(!funcDefined('requestCredit')){
        requestCredit =function( form ) {
        var form_url = "/ajax/ajaxRequestCredit.php";//$(form).attr('action');
        $.ajax({
            url: form_url, //$(form).attr('action'),
            data: $(form).serialize(),
            type: 'POST',
            dataType: 'html',
            error: function (data) {
                console.log(data);
            },
            success: function (data) {
                console.log(data);
            }
        });
        };
    }***/
</script>
