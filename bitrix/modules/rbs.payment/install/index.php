<?
use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);

class RBS_Payment extends CModule
{

    var $MODULE_ID = 'rbs.payment';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    protected $langMess;

    function __construct() {
        $path = realpath(dirname(dirname(__FILE__)));
        switch (SITE_CHARSET) {
            case 'windows-1251':
                $from = "/setup/cp1251/";
                include $path . '/setup/cp1251/lang/ru/config.php';
                break;
            default:
                $from = "/setup/utf8/";
                include $path . '/setup/utf8/lang/ru/config.php';
        }
        CopyDirFiles(
            $path . $from,
            $path ."/",
            true,
            true
        );


        require($path . "/config.php");

        $arModuleVersion = array();
        include __DIR__ . '/version.php';

        $this->MODULE_NAME = GetMessage('RBS0_MODULE_NAME') . " - " . RBS0_BANK_NAME;
        $this->MODULE_DESCRIPTION = GetMessage('RBS0_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('RBS0_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('RBS0_PARTNER_URI');

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
    }


    function DoInstall()
    {
        $this->InstallFiles();
        RegisterModule($this->MODULE_ID);
        COption::SetOptionInt($this->MODULE_ID, "delete", false);
    }


    function InstallFiles($arParams = array())
    {

        $path = realpath(dirname(dirname(__FILE__))) . "/install/sale_payment/payment/";
        $files = new DirectoryIterator($path);

        foreach ($files as $file) {
            // excluding the . and ..
            if ($file->isDot() === false) {
                // seek and replace ;)
                $path_to_file = $file->getPathname();
                $file_contents = file_get_contents($path_to_file);
                $file_contents = str_replace("{module_path}", $this->MODULE_ID, $file_contents);
                file_put_contents($path_to_file, $file_contents);
            }
        }

        CopyDirFiles(
            realpath(dirname(dirname(__FILE__))) . "/install/sale_payment/payment/",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/sale_payment/rbs_payment/"
        );

        CopyDirFiles(
            realpath(dirname(dirname(__FILE__))) . "/install/sale/payment/",
            $_SERVER["DOCUMENT_ROOT"] . "/sale/rbs_payment/"
        );

        CopyDirFiles(
            realpath(dirname(dirname(__FILE__))) . "/ajax.php",
            $_SERVER['DOCUMENT_ROOT'] . "/" . $this->MODULE_ID . "/ajax.php"
        );
    }


    function DoUninstall()
    {
        COption::SetOptionInt($this->MODULE_ID, "delete", true);
        DeleteDirFilesEx("/bitrix/php_interface/include/sale_payment/rbs_payment");
        DeleteDirFilesEx("/sale/rbs_payment/");
        DeleteDirFilesEx($this->MODULE_ID);

        UnRegisterModule($this->MODULE_ID);
        return true;
    }
}