<?
IncludeModuleLangFile(__FILE__);

// !!! ПОЖАЛУЙСТА НЕ ИСПОЛЬЗУЙТЕ В ЭТОМ ФАЙЛЕ КИРРИЛИЧЕСКИЕ СИМВОЛЫ !!!
// !!! PLEASE DO NOT USE INSIDE THIS FILE CYRILLIC SYMBOLS !!!

define('RBS0_BANK_NAME', 'Sberbank'); //
define('RBS0_MODULE_ID', 'rbs.payment');

define('RBS0_API_PROD_URL', 'https://securepayments.sberbank.ru/payment/rest/');
define('RBS0_API_TEST_URL', 'https://3dsec.sberbank.ru/payment/rest/');

define('RBS0_API_RETURN_PAGE', '/sale/rbs_payment/result.php');
define('RBS0_API_GATE_TRY',30);

define('RBS0_VERSION','3.2.3');

$status = COption::GetOptionString("rbs.payment", "result_order_status", "P");
if (!defined('RBS0_RESULT_ORDER_STATUS'))
    define('RBS0_RESULT_ORDER_STATUS', $status);

define('RBS0_DEFAULT_PAYMENT_OBJECT', 1);

$arDefaultIso = array(
    'USD' => 840,
    'EUR' => 978,
    'RUB' => 643,
    'RUR' => 643,
);

if (!defined('DEFAULT_ISO'))
    define(DEFAULT_ISO, serialize($arDefaultIso));