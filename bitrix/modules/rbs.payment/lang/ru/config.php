<?
$MESS["RBS0_MODULE_NAME"] = "Оплата картой банка";
$MESS["RBS0_MODULE_DESCRIPTION"] = "Модуль приема платежей позволяет упростить подключение к платежному шлюзу Банка для приема банковских карт Международных Платежных Систем (Visa, MasterCard, Мир) на вашем сайте";
$MESS["RBS0_PARTNER_NAME"] = "RBSPayment";
$MESS["RBS0_PARTNER_URI"] = "http://www.rbspayment.ru/";