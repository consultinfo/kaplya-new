<?
$MESS['RBS0_PAYMENT_PAY_SUM'] = 'Сумма к оплате по счету: ';
$MESS['RBS0_PAYMENT_PAY_BUTTON'] = 'Оплатить';
$MESS['RBS0_PAYMENT_PAY_REDIRECT'] = 'Вы будете перенаправлены на страницу оплаты';
$MESS['RBS0_PAYMENT_PAY_DESCRIPTION'] = '<b>Обратите внимание:</b> если вы откажетесь от покупки, для возврата денег вам придется обратиться в магазин.';
$MESS['RBS0_PAYMENT_PAY_ERROR'] = 'Неизвестная ошибка. Попробуйте оплатить заказ позднее.';
$MESS['RBS0_PAYMENT_PAY_ERROR_NUMBER'] = 'Ошибка №';
$MESS['RBS0_PAYMENT_DELIVERY_TITLE'] = 'Доставка';
$MESS['RBS0_PAYMENT_MEASURE_DEFAULT'] = 'Штука';
?>