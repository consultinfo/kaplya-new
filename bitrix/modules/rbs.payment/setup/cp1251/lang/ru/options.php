<?
$MESS["RBS0_RESULT_ORDER_STATUS"] = "������ ������ ��� �������� ������";
$MESS["RBS0_CHECK_HTTPS"] = "�������� ������ PHP; cURL; TLS";
$MESS['RBS0_CURRENCY_CHOISE'] = "����� �����";
$MESS['RBS0_CC_HEAD_CURRENCY'] = "������";
$MESS['RBS0_CC_HEAD_CODE'] = "���";
$MESS['RBS0_CC_HEAD_ISO'] = "ISO";
$MESS['RBS0_BANK_ISSUED_CHECK'] = "��� ��������� ����";
$MESS['RBS0_BANK_ISSUED_CHECK_DESCRIPTION'] = "����� �������� 'Y', �� ���������� � �������� ������� ���. ����� �������, <br>�� ������������ ���������� � ��������� ������ �����. <br>��� ������������� ���������� ��������� ��� ����������� �������";
$MESS['RBS0_TAX_SYSTEM'] = "������� ���������������";
$MESS['RBS0_TAX_SYSTEM_GENERAL'] = "�����";
$MESS['RBS0_TAX_SYSTEM_SIMPLIFIED_INCOME'] = "����������, �����";
$MESS['RBS0_TAX_SYSTEM_SIMPLIFIED_REVENUE_MINUS_CONSUMPTION'] = "����������, ����� ����� ������";
$MESS['RBS0_TAX_SYSTEM_SINGLE_TAX_ON_IMPUTED_INCOME'] = "������ ����� �� ��������� �����";
$MESS['RBS0_TAX_SYSTEM_UNIFIED_AGRICULTURAL_TAX'] = "������ �������������������� �����";
$MESS['RBS0_TAX_SYSTEM_PATENT_SYSTEM_OF_TAXATION'] = "��������� ������� ���������������";
$MESS['RBS0_TAB1_CURRENCY_TITLE'] = "������";
$MESS['RBS0_TAB1_FISCALIZATION_TITLE'] = "���";
$MESS['RBS0_TAB1_VAT_TITLE'] = "������ ���, ��� �������";
$MESS['RBS0_TAB1_VAT_NOT_SET'] = "-�� �������-";
$MESS['RBS0_TAB1_VAT_LIST_VALUE_0'] = "��� ���";
$MESS['RBS0_TAB1_VAT_LIST_VALUE_1'] = "��� 0%";
$MESS['RBS0_TAB1_VAT_LIST_VALUE_2'] = "��� 10%";
$MESS['RBS0_TAB1_VAT_LIST_VALUE_3'] = "��� 18%";
$MESS['RBS0_TAB1_VAT_DELIVERY_TITLE'] = "������ ���, ��� ��������";
$MESS['RBS0_ADVANCED_OPTIONS_TITLE'] = "����������� �����";
$MESS['RBS0_RETURN_PAGE_LABEL'] = "�������� ��������";
$MESS['RBS0_RETURN_PAGE_DESCRIPTION'] = "�������� ������ � ��� ������, ���� ��� ���������� <br>����������� ����������� ������ ��������� �������. ��� ����������. <br> �� ��������� - <b>/sale/rbs_payment/result.php</b>";

$MESS['RBS0_GATE_SEND_COMMENT_LABEL'] = "�����������";
$MESS['RBS0_GATE_SEND_COMMENT_NAME_FIO'] = "��� ����������";
$MESS['RBS0_GATE_SEND_COMMENT_NAME_COMMENT'] = "����������� � ������";
$MESS['RBS0_GATE_SEND_COMMENT_DESCRIPTION'] = "���� ������� ����� �������� � ������ ������� ����� � ���� description. <br>�� ���������, ������ ���������� <b>�����������</b> � ������";

$MESS['RBS0_GATE_TRY_LABEL'] = "���������� �������";
$MESS['RBS0_GATE_TRY_DESCRIPTION'] = "���������� ������� ����������� ������ �� �����. ����� ���� ��� ������ �������� �������� �� �������� ��������� ���������� ������. ����� �������������� �� ����������� �����, ���� ������������ ����� ��������� ��������, �� ����� ���������������� ����� �������. �� ���������: 30 �������.";
?>