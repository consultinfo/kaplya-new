<?
$MESS['RBS0_PAYMENT_PAY_FROM'] = '������ ���������� ������';
$MESS['RBS0_PAYMENT_LOGIN'] = '�����';
$MESS['RBS0_PAYMENT_PASSWORD'] = '������';
$MESS['RBS0_PAYMENT_STAGING'] = '������������� �������';
$MESS['RBS0_PAYMENT_STAGING_DESC'] = '���� �������� \'Y\', ����� ������������� ������������� ������. ��� ������ �������� ����� ������������� ������������� ������.';
$MESS['RBS0_PAYMENT_TEST_MODE'] = '�������� �����';
$MESS['RBS0_PAYMENT_TEST_MODE_DESC'] = '���� �������� \'Y\', ������ ����� �������� � �������� ������. ��� ������ �������� ����� ����������� ����� ������.';
$MESS['RBS0_PAYMENT_LOGGING'] = '�����������';
$MESS['RBS0_PAYMENT_LOGGING_DESC'] = '���� �������� \'Y\', ������ ����� ���������� ���� ������ � ����. ��� ������ �������� ����������� ����������� �� �����.';
$MESS['RBS0_PAYMENT_ACCOUNT_NUMBER'] = '���������� ������������� ������ � ��������';
$MESS['RBS0_PAYMENT_ORDER_SUM'] = '����� ������';
$MESS['RBS0_PAYMENT_SHIPMENT_NAME'] = '��������� ��������';
$MESS['RBS0_PAYMENT_SHIPMENT_DESC'] = '���� �������� \'Y\', �� ����� �������� ������ ����� ������������� ��������� �������� ������.';
$MESS['RBS0_PAYMENT_SET_PAYED'] = '������������� ������ �������';
$MESS['RBS0_PAYMENT_SET_PAYED_DESC'] = '���� �������� \'Y\', �� ����� �������� ������ ����� ����� ��������� � ������ �������.';
$MESS['RBS0_PAYMENT_CHECK'] = '��� ��������� ����';
$MESS['RBS0_PAYMENT_CHECK_DESC'] = '���� �������� \'Y\', �� � ��������� ���� ����� ������� ������ ������� ��� ������������ ���� ';
$MESS['RBS0_PAYMENT_AUTO_OPEN_FORM'] = '�������������� ������� � ����� ������';
$MESS['RBS0_PAYMENT_AUTO_OPEN_FORM_DESC'] = '���� �������� \'Y\', �� ����� ���������� ������, ���������� ����� ������������� ������������� �� �������� ��������� �����. \'����������:\' �������� ������ � ���������� ������� "��������� � ����� ����."';
$MESS['RBS0_PAYMENT_FFD_FORMAT'] = '������ ���������� ����������';
$MESS['RBS0_PAYMENT_FFD_FORMAT_DESC'] = '������ ������ ��������� ������� � ������ �������� ����� � � �������� ������� ������������';


$MESS['RBS0_PAYMENT_FFD_FORMAT'] = '������ ���������� ����������';
$MESS['RBS0_PAYMENT_FFD_FORMAT_DESC'] = '������ ������ ��������� ������� � ������ �������� ����� � � �������� ������� ������������';

$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_NAME'] = "��� 1.05 ��� ������:";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_DESCR'] = "������ �������� ��������� ��� ������ ������ ��� 1.05";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_1'] = "������ ��������������� ������ �� ������� �������� �������� �������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_2'] = "��������� ��������������� ������ �� ������� �������� �������� �������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_3'] = "�����";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_4'] = "������ ������ � ������ �������� �������� �������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_5'] = "��������� ������ �������� ������� � ������ ��� �������� � ����������� ������� � ������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_6'] = "�������� �������� ������� ��� ��� ������ � ������ ��� �������� � ����������� ������� � ������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_METHOD_7'] = "������ �������� ������� ����� ��� �������� � ������� � ������";

$MESS['RBS0_PAYMENT_FFD_PAYMENT_OBJECT_NAME'] = "��� ������������ �������";
$MESS['RBS0_PAYMENT_FFD_PAYMENT_OBJECT_DESCR'] = "������ �������� ��������� ��� ������ ������ ��� 1.05";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_1"]  = "�����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_2"]  = "����������� �����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_3"]  = "������";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_4"]  = "������";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_5"]  = "������ �������� ����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_6"]  = "������� �������� ����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_7"]  = "���������� �����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_8"]  = "������� �������";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_9"]  = "�������������� ���";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_10"] = "�����";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_11"] = "��������� ��������������";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_12"] = "��������� ������� �������";
$MESS["RBS0_PAYMENT_FFD_PAYMENT_OBJECT_13"] = "���� ������� �������";
?>