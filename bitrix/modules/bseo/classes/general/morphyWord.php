<?php

class morphyWord {

    private static $instance;
    private $morphy;
    private $charset;

    protected function __construct($charset = 'utf-8') {
        $this->charset = $charset;
        $opts = Array(
            'storage' => PHPMORPHY_STORAGE_FILE
        );
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/bseo/classes/phpmorphy/dicts";
        $lang = 'ru_RU';

        try {
            $this->morphy = new phpMorphy($dir, $lang, $opts);
        } catch(phpMorphy_Exception $e) {
            die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        }
    }

    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    private function toLower($word) {
        return mb_strtolower($word, $this->charset);
    }

    private function toUpper($word) {
        return mb_strtoupper($word, $this->charset);
    }

    public function getFormsWord($word, $type = 'ЕД') {

        $word = $this->toUpper($word);

        $paradigms = $this->morphy->findWord($word);
        $word_forms = array();

        if($paradigms) {
            $paradigms->getByPartOfSpeech('С');

            foreach ($paradigms as $paradigm) {
                $info = $paradigm->getFoundWordForm();
                if(isset($info[0])) {
                    $gramm = $info[0]->getGrammems();
                    if(in_array("МН", $gramm)) {
                        $type = "МН";
                    }
                }
                foreach ($paradigm as $form) {

                    $value = $this->toLower($form->getWord());

                    if ($form->hasGrammems(array($type, 'РД'))) {
                        $word_forms['GENITIVE'] = $value;
                    } elseif ($form->hasGrammems(array($type, 'ДТ'))) {
                        $word_forms['DATIVE'] = $value;
                    } elseif ($form->hasGrammems(array($type, 'ВН'))) {
                        $word_forms['ACCUSATIVE'] = $value;
                    } elseif ($form->hasGrammems(array($type, 'ТВ'))) {
                        $word_forms['ABLATIVE'] = $value;
                    } elseif ($form->hasGrammems(array($type, 'ПР'))) {
                        $word_forms['PREPOSITIONAL'] = $value;
                    }
                }
            }
        }

        return $word_forms;
    }

    public function getFormsPhrase($arWords) {
        if(!is_array($arWords))
            $arWords = explode(',', $arWords);

        $phase_forms = array();
        foreach($arWords as $arWord){
            $item = trim($arWord);
            if(!$item)
                continue;

            if(stripos($item, " ") === false) {
                $forms = $this->getFormsWord($item);

                $phase_forms['GENITIVE'][]          = $forms['GENITIVE'];
                $phase_forms['DATIVE'][]            = $forms['DATIVE'];
                $phase_forms['ACCUSATIVE'][]        = $forms['ACCUSATIVE'];
                $phase_forms['ABLATIVE'][]          = $forms['ABLATIVE'];
                $phase_forms['PREPOSITIONAL'][]     = $forms['PREPOSITIONAL'];
            } else {
                $items = explode(' ', $item);
                $words = Array();

                foreach($items as $text){
                    $text = trim($text);
                    if(!$text)
                        continue;

                    $forms = $this->getFormsWord($text);

                    $words['GENITIVE'][]            = $forms['GENITIVE'];
                    $words['DATIVE'][]              = $forms['DATIVE'];
                    $words['ACCUSATIVE'][]          = $forms['ACCUSATIVE'];
                    $words['ABLATIVE'][]            = $forms['ABLATIVE'];
                    $words['PREPOSITIONAL'][]       = $forms['PREPOSITIONAL'];
                }
                foreach ($words as $key => $value) {
                    $phase_forms[$key][] = implode(" ", $words[$key]);
                }
            }
        }

        return $phase_forms;
    }
}