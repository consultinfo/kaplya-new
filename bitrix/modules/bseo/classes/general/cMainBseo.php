<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/bseo/classes/phpmorphy/common.php";

class cMainBseo {
    static $MODULE_ID = "bseo";

    function OnEndBufferContentHandler (&$content) {
        global $APPLICATION;

        if($text = $APPLICATION->GetProperty('text', '')) {
            $content = preg_replace('/<!-- PATTERN -->(.+)<!-- END_PATTERN -->/siU', $text, $content);
        }
    }

    function OnBeforeEndBufferContentHandler () {
        global $APPLICATION, $DB;

        $url = $_SERVER['REQUEST_URI'];
        $domain = $_SERVER['HTTP_HOST'];
        $subdomain = false;
        $strWhere = "";

        if(mb_substr_count($domain, '.') == 2) {
            list($subdomain, $domain) = explode('.', $domain, 2);
        }

        if($subdomain == false || mb_strtolower($subdomain) == "www") {
            $strWhere = "(SUBDOMAIN = '' OR SUBDOMAIN IS NULL OR SUBDOMAIN = 'www') AND";
        } else {
            $strWhere = "(SUBDOMAIN = '' OR SUBDOMAIN IS NULL OR SUBDOMAIN = '" . $DB->ForSql($subdomain) . "') AND";
        }

        $dbResult = $DB->Query("SELECT * FROM b_bseo_tags WHERE {$strWhere} PAGE_URL = '" . $DB->ForSql($url) . "' ORDER BY SUBDOMAIN DESC LIMIT 1;");

        if($arItem = $dbResult->Fetch()) {
            if(!empty($arItem["PAGE_TITLE"])) {
                $APPLICATION->SetPageProperty('title', $arItem['PAGE_TITLE']);
            }
            if(!empty($arItem["PAGE_KEYWORDS"])) {
                $APPLICATION->SetPageProperty('keywords', $arItem['PAGE_KEYWORDS']);
            }
            if(!empty($arItem["PAGE_DESCRIPTION"])) {
                $APPLICATION->SetPageProperty('description', $arItem['PAGE_DESCRIPTION']);
            }
            if(!empty($arItem["PAGE_TEXT"])) {
                $APPLICATION->SetPageProperty('text', $arItem['PAGE_TEXT']);
            }
            if(!empty($arItem["PAGE_HEAD"])) {
                $APPLICATION->SetTitle($arItem['PAGE_HEAD']);
            }
        } else {
            $FILTER_NAME = COption::GetOptionString(self::$MODULE_ID, "FILTER_NAME");
            global $$FILTER_NAME;
            
            $arFilter = isset($$FILTER_NAME) ? $$FILTER_NAME : Array();
            if (isset($$FILTER_NAME) && $$FILTER_NAME && (count($arFilter) > 1 || !isset($arFilter["FACET_OPTIONS"]))) {

                $IBLOCK_ID = COption::GetOptionString(self::$MODULE_ID, "IBLOCK_ID");
                $FILTER_HEAD = COption::GetOptionString(self::$MODULE_ID, "FILTER_HEAD");
                $FILTER_TITLE = COption::GetOptionString(self::$MODULE_ID, "FILTER_TITLE");
                $FILTER_DESCRIPTION = COption::GetOptionString(self::$MODULE_ID, "FILTER_DESCRIPTION");

                $arTParams = self::parseParams($FILTER_HEAD);
                $arTParams = self::parseParams($FILTER_TITLE);
                $arDParams = self::parseParams($FILTER_DESCRIPTION);

                if (isset($arTParams["CATEGORY"]) || isset($arDParams["CATEGORY"])) {
                    $FILTER_CATEGORY = COption::GetOptionString(self::$MODULE_ID, "FILTER_CATEGORY");

                    $pattern = preg_replace('/\#.+\#/U', '(.+)', $FILTER_CATEGORY);
                    $pattern = "/" . addcslashes($pattern, '/') . "/";

                    preg_match($pattern, $_SERVER["REQUEST_URI"], $arOut);
                    if ($arOut && isset($arOut[1])) {
                        $category_path = $arOut[1];
                        list($category_code) = explode("/", $category_path);

                        $rsSection = CIBlockSection::GetList(Array(), Array("CODE" => $category_code, "IBLOCK_ID" => $IBLOCK_ID));
                        if ($arSection = $rsSection->GetNext()) {
                            if (isset($arTParams["CATEGORY"]))
                                $arTParams["CATEGORY"]["VALUE"] = $arSection["NAME"];
                            if (isset($arDParams["CATEGORY"]))
                                $arDParams["CATEGORY"]["VALUE"] = $arSection["NAME"];
                        }
                    }
                }

                if($FILTER_HEAD) {
                    self::prepareParams($arTParams, $arFilter);
                    $text = self::insertParams($arTParams, $FILTER_HEAD);
                    $APPLICATION->SetTitle($text);
                }

                if($FILTER_TITLE) {
                    self::prepareParams($arTParams, $arFilter);
                    $text = self::insertParams($arTParams, $FILTER_TITLE);
                    $APPLICATION->SetPageProperty('title', $text);
                }

                if($FILTER_DESCRIPTION) {
                    self::prepareParams($arDParams, $arFilter);
                    $text = self::insertParams($arDParams, $FILTER_DESCRIPTION);
                    $APPLICATION->SetPageProperty('description', $text);
                }
            }
        }
    }

    public static function eachParams($items, &$arParams) {
        foreach ($items as $code => $values) {
            if($code != "OFFERS") {
                foreach ($arParams as $p => $arParam) {
                    if ($code == "=PROPERTY_" . $arParam["ID"] || $p == $code) {
                        $rsProp = CIBlockProperty::GetByID($arParam["ID"]);
                        if ($arProp = $rsProp->GetNext()) {

                            if ($arProp["PROPERTY_TYPE"] == "L") {
                                if(stripos($arParam["PARAMS"], "-N") !== false) {
                                    $arParams[$p]["VALUE"] = $arProp["NAME"];
                                } else {
                                    $arParams[$p]["VALUE"] = Array();

                                    if(is_array($values)) {
                                        foreach ($values as $value) {
                                            $rsEnum = CIBlockProperty::GetPropertyEnum($arParam["ID"], Array(), Array("ID" => $value));
                                            if($arEnum = $rsEnum->GetNext())
                                                $arParams[$p]["VALUE"][] = $arEnum["VALUE"];
                                        }
                                    } else {
                                        $rsEnum = CIBlockProperty::GetPropertyEnum($arParam["ID"], Array(), Array("ID" => $values));
                                        if($arEnum = $rsEnum->GetNext())
                                            $arParams[$p]["VALUE"][] = $arEnum["VALUE"];
                                    }
                                }
                            }

                            if ($arProp["PROPERTY_TYPE"] == "E") {
                                $arParams[$p]["VALUE"] = Array();
                                if(is_array($values)) {
                                    foreach ($values as $value) {
                                        $rsElement = CIBlockElement::GetByID($value);
                                        if($arElement = $rsElement->GetNext())
                                            $arParams[$p]["VALUE"][] = $arElement["NAME"];
                                    }
                                } else {
                                    $rsElement = CIBlockElement::GetByID($values);
                                    if($arElement = $rsElement->GetNext())
                                        $arParams[$p]["VALUE"][] = $arElement["NAME"];
                                }
                            }

                            if ($arProp["PROPERTY_TYPE"] == "S") {
                                $arParams[$p]["VALUE"] = $values;
                            }
                        }
                    }
                }
            }
        }
    }

    public static function prepareParams(&$arParams, $arFilter) {
        if(isset($arFilter["PROPERTY"])) {
            self::eachParams($arFilter["PROPERTY"], $arParams);
        } else {
            self::eachParams($arFilter, $arParams);
            self::eachParams($arFilter["OFFERS"], $arParams);
        }
    }

    public static function insertParams($arParams, $string) {
        preg_match_all('/{.+}/U', $string, $params);
        if($params && $params[0]) {
            $value = array_values($arParams);
            foreach ($params[0] as $key => $item) {
                if($value[$key]) {
                    if(empty($value[$key]["VALUE"])) {
                        $text = "";
                        if(!trim($value[$key]["VALUE"])) {
                            $text = $value[$key]["EMPTY"];
                        }

                        $string = str_replace($item, $text, $string);
                    } else {
                        $objWord = morphyWord::getInstance();

                        $v = $value[$key]["VALUE"];

                        $cases = Array("-PG", "-PD", "-PAC", "-PAB", "-PP");
                        $cases = self::multineedle_stripos($value[$key]["PARAMS"], $cases);

                        foreach ($cases as $name => $case) {
                            if($case !== false) {
                                $forms = $objWord->getFormsPhrase($v);

                                switch ($name) {
                                    case "-PG":
                                        $v = implode(", ", $forms["GENITIVE"]);
                                        break;
                                    case "-PD":
                                        $v = implode(", ", $forms["DATIVE"]);
                                        break;
                                    case "-PAC":
                                        $v = implode(", ", $forms["ACCUSATIVE"]);
                                        break;
                                    case "-PAB":
                                        $v = implode(", ", $forms["ABLATIVE"]);
                                        break;
                                    case "-PP":
                                        $v = implode(", ", $forms["PREPOSITIONAL"]);
                                        break;
                                }
                            }
                        }

                        if(is_array($v)) {
                            $v = implode(", ", $v);
                        }

                        if(stripos($value[$key]["PARAMS"], "-L") !== false) {
                            $v = mb_strtolower($v);
                        }

                        if(stripos($value[$key]["PARAMS"], "-C") !== false) {
                            $v = self::mb_strtoupper_first($v);
                        }

                        $text = $value[$key]["PREFIX"] . $v . $value[$key]["POSTFIX"];

                        $string = str_replace($item, $text, $string);
                    }

                } else {
                    $string = str_replace($item, "", $string);
                }
            }
        }

        return $string;
    }
    function mb_strtoupper_first($str, $encoding = 'UTF8') {
        return mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) . mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
    }

    public static  function multineedle_stripos($haystack, $needles, $offset=0) {
        foreach($needles as $needle) {
            $found[$needle] = stripos($haystack, $needle, $offset);
        }
        return $found;
    }

    public static function parseParams($string) {
        $arReplace = Array();

        preg_match_all('/{.+}/U', $string, $params);
        if($params && $params[0]) {
            foreach ($params[0] as $param) {
                $tmp = $param;
                $tmp = str_replace("{", "", $tmp);
                $tmp = str_replace("}", "", $tmp);

                list($tmp, $sEmpty) = explode("#", $tmp);
                list($tmp, $sParam) = explode("!", $tmp);

                $tmp = explode("|", $tmp);

                $key = 0;
                foreach ($tmp as $k => $item) {
                    if (stripos($item, ".")) {
                        $key = $k;
                    }
                }

                if (stripos($tmp[$key], ".")) {
                    list($sParamID, $sCODE) = explode(".", $tmp[$key]);
                } else {
                    $sParamID = 0;
                    $sCODE = $tmp[$key];
                }

                $arReplace[$sCODE] = Array(
                    "ID" => $sParamID,
                    "CODE" => $sCODE,
                    "PREFIX" => ($key > 0 && isset($tmp[$key - 1])) ? $tmp[$key - 1] : "",
                    "POSTFIX" => (($key == 0 || ($key == 1 && count($tmp) > 2)) && isset($tmp[$key + 1])) ? $tmp[$key + 1] : "",
                    "PARAMS" => $sParam ? $sParam : "",
                    "EMPTY" => $sEmpty ? $sEmpty : ""
                );
            }
        }
        return $arReplace;
    }

    public static function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu) {

        $aModuleMenu[] = Array(
            "parent_menu"   => "global_menu_content",
            "icon"          => "util_menu_icon",
            "page_icon"     => "util_menu_icon",
            "sort"          => "0",
            "text"          => "Bitrix SEO помощник",
            "title"         => "Bitrix SEO помощник",
            "url"           => "/bitrix/admin/bseo_tags.php",
            "more_url"      => Array(),
            "items"         => Array(
                Array(
                    "text" => "Список тегов",
                    "url" => "/bitrix/admin/bseo_tags.php",
                    "more_url" => Array()
                ),
                Array(
                    "text" => "Настройки",
                    "url" => "/bitrix/admin/settings.php?mid=bseo",
                    "more_url" => Array()
                )
            )
        );
    }
}