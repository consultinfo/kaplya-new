<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/prolog.php");

IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("bseo");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Общие", "ICON" => "main_user_edit", "TITLE" => "Форма добавление тегов")
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$sTableID = "b_bseo_tags";

$ID = intval($ID);
$message = null;
$bVarsFromForm = false;

if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $POST_RIGHT=="W" && check_bitrix_sessid()) {

    $arUrl = parse_url($PAGE_URL);

    $PAGE_URL = $arUrl["path"] . (isset($arUrl["query"]) ? "?" . $arUrl["query"] : "");

    $rsData = $DB->Query("SELECT * FROM b_bseo_tags WHERE SUBDOMAIN = ". "'" . $DB->ForSql(trim($SUBDOMAIN)) . "'" ." AND PAGE_URL = " . "'" . $DB->ForSql(trim($PAGE_URL)) . "'" . ";");
    if(($arItem = $rsData->Fetch()) && $arItem["ID"] != $ID) {
        $res = false;
        $message = new CAdminMessage("Ошибка сохранения элемента, такой URL уже существует");
        $bVarsFromForm = true;
    } else {

        $DB->PrepareFields($sTableID);
        $arFields = Array(
            "SUBDOMAIN" => "'" . $DB->ForSql(trim($SUBDOMAIN)) . "'",
            "PAGE_URL" => "'" . $DB->ForSql(trim($PAGE_URL)) . "'",
            "PAGE_TITLE" => "'" . $DB->ForSql(trim($PAGE_TITLE)) . "'",
            "PAGE_DESCRIPTION" => "'" . $DB->ForSql(trim($PAGE_DESCRIPTION)) . "'",
            "PAGE_KEYWORDS" => "'" . $DB->ForSql(trim($PAGE_KEYWORDS)) . "'",
            "PAGE_HEAD" => "'" . $DB->ForSql(trim($PAGE_HEAD)) . "'",
            "PAGE_TEXT" => "'" . $DB->ForSql(trim($PAGE_TEXT)) . "'"
        );

        $DB->StartTransaction();
        if ($ID > 0) {
            $res = $DB->Update($sTableID, $arFields, "WHERE ID = '{$ID}'", $err_mess . __LINE__);
        } else {
            $ID = $DB->Insert($sTableID, $arFields, $err_mess . __LINE__);
            $res = ($ID > 0);
        }
        if (strlen($strError) <= 0) {
            $DB->Commit();
            $res = true;
        } else {
            $DB->Rollback();
        }

        if ($res) {
            if ($apply != "")
                LocalRedirect("/bitrix/admin/bseo_tags_edit.php?ID=" . $ID . "&mess=ok&lang=" . LANG . "&" . $tabControl->ActiveTabParam());
            else
                LocalRedirect("/bitrix/admin/bseo_tags.php?lang=" . LANG);
        } else {
            if ($e = $APPLICATION->GetException())
                $message = new CAdminMessage("Ошибка сохранения элемента", $e);
            $bVarsFromForm = true;
        }
    }
}

$str_SUBDOMAIN          = "";
$str_PAGE_URL           = "";
$str_PAGE_TITLE         = "";
$str_PAGE_DESCRIPTION   = "";
$str_PAGE_KEYWORDS      = "";
$str_PAGE_HEAD          = "";
$str_PAGE_TEXT          = "";

if($ID>0) {
    $rsData = $DB->Query("SELECT * FROM b_bseo_tags WHERE ID = {$ID};");
    if(!$rsData->ExtractFields("str_"))
        $ID=0;
}

if($bVarsFromForm)
    $DB->InitTableVarsForEdit("b_bseo_tags", "", "str_");

$APPLICATION->SetTitle(($ID>0? "Редактирование записи: " . $ID : "Добавление записи"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
    array(
        "TEXT"  => "Список тегов",
        "TITLE" => "Список тегов",
        "LINK"  => "bseo_tags.php?lang=".LANG,
        "ICON"  => "btn_list",
    )
);

if($ID>0)
{
    $aMenu[] = array("SEPARATOR"=>"Y");
    $aMenu[] = array(
        "TEXT"  => "Добавить элемент",
        "TITLE" => "Добавить элемент",
        "LINK"  => "rubric_edit.php?lang=".LANG,
        "ICON"  => "btn_new",
    );
    $aMenu[] = array(
        "TEXT"  => "Удалить элемент",
        "TITLE" => "Удалить элемент",
        "LINK"  => "javascript:if(confirm('"."Вы уверены что хостите удалить элемент?"."'))window.location='rubric_admin.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."';",
        "ICON"  => "btn_delete",
    );
}

$context = new CAdminContextMenu($aMenu);

$context->Show();

if($_REQUEST["mess"] == "ok" && $ID>0)
    CAdminMessage::ShowMessage(Array("MESSAGE" => "Теги сохранены", "TYPE" => "OK"));

if($message)
    echo $message->Show();
elseif($DB->GetErrorMessage()!="")
    CAdminMessage::ShowMessage($DB->GetErrorMessage());
?>
    <form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
        <?= bitrix_sessid_post() ?>
        <? $tabControl->Begin(); ?>
        <? $tabControl->BeginNextTab(); ?>
        <tr>
            <td>Поддомен</td>
            <td><input type="text" name="SUBDOMAIN" value="<?echo $str_SUBDOMAIN;?>" size="50"></td>
        </tr>
        <tr>
            <td></td>
            <td>
                Пустой - для основного домена и любых поддоменов<br/>
                www - только для основного домена<br/>
                Наименование поддомен (пример omsk, nsk, subdomain и т.д.)
            </td>
        </tr>
        <tr>
            <td>URL</td>
            <td><input type="text" name="PAGE_URL" value="<?echo $str_PAGE_URL;?>" size="50"></td>
        </tr>
        <tr>
            <td>Title</td>
            <td><input type="text" name="PAGE_TITLE" value="<?echo $str_PAGE_TITLE;?>" size="50"></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name="PAGE_DESCRIPTION" cols="52"><?echo $str_PAGE_DESCRIPTION;?></textarea></td>
        </tr>
        <tr>
            <td>Keywords</td>
            <td><input type="text" name="PAGE_KEYWORDS" value="<?echo $str_PAGE_KEYWORDS;?>" size="50"></td>
        </tr>
        <tr>
            <td>H1</td>
            <td><input type="text" name="PAGE_HEAD" value="<?echo $str_PAGE_HEAD;?>" size="50"></td>
        </tr>
        <tr>
            <td>Text</td>
            <td>
                <?$APPLICATION->IncludeComponent("bitrix:fileman.light_editor","",Array(
                        "CONTENT" => $str_PAGE_TEXT,
                        "INPUT_NAME" => "PAGE_TEXT",
                        "INPUT_ID" => "",
                        "WIDTH" => "100%",
                        "HEIGHT" => "300px",
                        "RESIZABLE" => "Y",
                        "AUTO_RESIZE" => "Y",
                        "VIDEO_ALLOW_VIDEO" => "Y",
                        "VIDEO_MAX_WIDTH" => "640",
                        "VIDEO_MAX_HEIGHT" => "480",
                        "VIDEO_BUFFER" => "20",
                        "VIDEO_LOGO" => "",
                        "VIDEO_WMODE" => "transparent",
                        "VIDEO_WINDOWLESS" => "Y",
                        "VIDEO_SKIN" => "/bitrix/components/bitrix/player/mediaplayer/skins/bitrix.swf",
                        "USE_FILE_DIALOGS" => "Y",
                        "ID" => "",
                        "JS_OBJ_NAME" => ""
                    )
                );?>
                <br/>
                <br/>
                &#x3C;!-- PATTERN --&#x3E; Замена &#x3C;!-- END_PATTERN --&#x3E;
            </td>
        </tr>

        <?
        $tabControl->Buttons(
            array(
                "disabled" => ($POST_RIGHT<"W"),
                "back_url" => "bseo_tags.php?lang=".LANG,
            )
        );
        ?>

        <input type="hidden" name="lang" value="<?=LANG?>">
        <?if($ID>0 && !$bCopy):?>
            <input type="hidden" name="ID" value="<?=$ID?>">
        <?endif;?>
        <? $tabControl->End(); ?>
        <? $tabControl->ShowWarnings("post_form", $message); ?>
    </form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");