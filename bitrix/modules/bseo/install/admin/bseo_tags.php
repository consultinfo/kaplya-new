<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/subscribe/prolog.php");

IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("bseo");
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "b_bseo_tags";

$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

function CheckFilter() {
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f) global $$f;

    return count($lAdmin->arFilterErrors) == 0;
}

$FilterArr = Array(
    "find"
);

$lAdmin->InitFilter($FilterArr);

$sWhere = "";
if (CheckFilter()) {
    $sWhere .= "WHERE PAGE_URL LIKE '%{$find}%'";
}


if($lAdmin->EditAction() && $POST_RIGHT=="W") {

    foreach($FIELDS as $ID => $arFields) {
        if(!$lAdmin->IsUpdated($ID)) {
            continue;
        }

        $DB->StartTransaction();
        $ID = IntVal($ID);

        if(($rsData = $DB->Query("SELECT * FROM b_bseo_tags WHERE ID = {$ID};")) && ($arData = $rsData->Fetch())) {
            foreach($arFields as $key=>$value) {
                $arData[$key] = "'" . $DB->ForSql(trim($value)) . "'";
            }

            if(!$DB->Update($sTableID, $arData, "WHERE ID = '{$ID}'")) {
                $lAdmin->AddGroupError("Ошибка сохранения элемента" . " " . $cData->LAST_ERROR, $ID);
                $DB->Rollback();
            }
        } else {
            $lAdmin->AddGroupError("Ошибка сохранения элемента", $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

if(($arID = $lAdmin->GroupAction()) && $POST_RIGHT=="W") {
    if($_REQUEST['action_target'] == 'selected') {
        $arID = Array();
        $rsData = $DB->Query("SELECT ID FROM b_bseo_tags;");
        while($arRes = $rsData->Fetch()) {
            $arID[] = $arRes['ID'];
        }
    }

    foreach($arID as $ID) {
        if(strlen($ID)<=0) {
            continue;
        }

        $ID = IntVal($ID);

        switch($_REQUEST['action']) {
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if(!$DB->Query("DELETE FROM b_bseo_tags WHERE ID = {$ID};")) {
                    $DB->Rollback();
                    $lAdmin->AddGroupError("Ошибка удаления записи", $ID);
                }
                $DB->Commit();
                break;
        }
    }
}


$rsData = $DB->Query("SELECT * FROM b_bseo_tags {$sWhere};");

$rsData = new CAdminResult($rsData, $sTableID);

$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint("Элементов"));

$lAdmin->AddHeaders(
    Array(
        Array(
            "id"        => "ID",
            "content"   => "ID",
            "sort"      => "id",
            "align"     => "right",
            "default"   => true,
        ),
        Array(
            "id"        => "SUBDOMAIN",
            "content"   => "SUBDOMAIN",
            "sort"      => "subdomain",
            "default"   => true,
        ),
        Array(
            "id"        => "PAGE_URL",
            "content"   => "URL",
            "sort"      => "PAGE_URL",
            "default"   => true,
        ),
        Array(
            "id"        => "PAGE_TITLE",
            "content"   => "Title",
            "sort"      => "PAGE_TITLE",
            "default"   => true,
        ),
        Array(
            "id"        => "PAGE_DESCRIPTION",
            "content"   => "Description",
            "sort"      => "PAGE_DESCRIPTION",
            "default"   => true,
        ),
        Array(
            "id"        => "PAGE_KEYWORDS",
            "content"   => "Keywords",
            "sort"      => "PAGE_KEYWORDS",
            "default"   => true,
        ),
        Array(
            "id"        => "PAGE_HEAD",
            "content"   => "H1",
            "sort"      => "PAGE_HEAD",
            "default"   => true,
        )
    )
);

while($arRes = $rsData->NavNext(true, "bseo_")) {
    $row =& $lAdmin->AddRow($bseo_ID, $arRes);

    /*$row->AddInputField("PAGE_URL",         Array("size" => 20));
    $row->AddInputField("PAGE_TITLE",       Array("size" => 20));
    $row->AddInputField("PAGE_DESCRIPTION", Array("size" => 20));
    $row->AddInputField("PAGE_KEYWORDS",    Array("size" => 20));
    $row->AddInputField("PAGE_HEAD",        Array("size" => 20));*/

    $arActions = Array();

    $arActions[] = array(
        "ICON"      => "edit",
        "DEFAULT"   => true,
        "TEXT"      => "Редактировать",
        "ACTION"    => $lAdmin->ActionRedirect("bseo_tags_edit.php?ID=".$bseo_ID)
    );

    if ($POST_RIGHT>="W")
        $arActions[] = array(
            "ICON"      => "delete",
            "TEXT"      => "Удалить",
            "ACTION"    => "if(confirm('" . "Вы уверены что хостите удалить элемент?" . "')) ".$lAdmin->ActionDoGroup($bseo_ID, "delete")
        );


    $row->AddActions($arActions);
}

$lAdmin->AddGroupActionTable(
    Array(
        "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE")
    )
);

$aContext = array(
    array(
        "TEXT"  => "Добавить",
        "LINK"  => "bseo_tags_edit.php?lang=".LANG,
        "TITLE" => "Добавить тег",
        "ICON"  => "btn_new",
    ),
);

$lAdmin->AddAdminContextMenu($aContext);

$APPLICATION->SetTitle("Bitrix SEO помощник");

$lAdmin->CheckListMode();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "URL"
    )
);
?>


    <form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
        <?$oFilter->Begin();?>
        <tr>
            <td><b>URL страницы:</b></td>
            <td>
                <input type="text" size="25" name="find" value="<?echo htmlspecialchars($find)?>" title="Поиск">
            </td>
        </tr>
        <?
        $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"));
        $oFilter->End();
        ?>
    </form>

<?
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");