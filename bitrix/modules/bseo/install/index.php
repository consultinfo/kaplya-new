<?php

class bseo extends CModule {

    var $MODULE_ID = "bseo";
    var $MODULE_VERSION = "1.0.0";
    var $MODULE_VERSION_DATE = "2018-11-28 00:00:00";
    var $MODULE_NAME = "Bitrix SEO помощник";
    var $MODULE_DESCRIPTION = "";
    var $MODULE_CSS;

    function bseo() {

    }

    function DoInstall() {
        global $DOCUMENT_ROOT, $APPLICATION, $DB;
        RegisterModule($this->MODULE_ID);

        if(file_exists(__DIR__ . "/install.sql")) {
            $intall_sql = file_get_contents(__DIR__ . "/install.sql");
            $dbResult = $DB->Query($intall_sql, true);
        }

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/bseo/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true);
        RegisterModuleDependences("main","OnBeforeEndBufferContent", "bseo", "cMainBseo", "OnBeforeEndBufferContentHandler");
        RegisterModuleDependences("main","OnBuildGlobalMenu", "bseo", "cMainBseo", "OnBuildGlobalMenu");
        RegisterModuleDependences("main","OnEndBufferContent", "bseo", "cMainBseo", "OnEndBufferContentHandler", 50);

        return true;
    }

    function DoUninstall() {
        global $DOCUMENT_ROOT, $APPLICATION, $DB;
        UnRegisterModule($this->MODULE_ID);

        if(file_exists(__DIR__ . "/uninstall.sql")) {
            $intall_sql = file_get_contents(__DIR__ . "/uninstall.sql");
            $dbResult = $DB->Query($intall_sql, true);
        }

        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/bseo/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");

        UnRegisterModuleDependences("main","OnBeforeEndBufferContent", "bseo", "cMainBseo", "OnBeforeEndBufferContentHandler");
        UnRegisterModuleDependences("main","OnBuildGlobalMenu", "bseo", "cMainBseo", "OnBuildGlobalMenu");
        UnRegisterModuleDependences("main","OnEndBufferContent", "bseo", "cMainBseo", "OnEndBufferContentHandler");

        return true;
    }
}