<?php

/**
 * Created by PhpStorm.
 * User: emas
 * Date: 06.09.2017
 * Time: 17:13
 */

namespace Mango;

use Bitrix\Main\Config\Option;

class DCTWidget
{
    public function onBeforeBufferEnd(&$content) {
        if (Option::get('mangotelecom.calltracking', 'SHOW_WIDGET') == 'Y') {
            $html = file_get_contents(dirname(__FILE__).'/../js/script.js');
            $html = str_replace('MANGO_WIDGET_ID', Option::get('mangotelecom.calltracking', 'WIDGET_ID'), $html);
            $html = str_replace('MANGO_WIDGET_SELECTOR', Option::get('mangotelecom.calltracking', 'PHONENUMBER_ELEM_SELECTOR'), $html);
            $html = '<script>'.$html.'</script>';
            $content = str_replace('</body>', $html.'</body>', $content);
        }
    }
}