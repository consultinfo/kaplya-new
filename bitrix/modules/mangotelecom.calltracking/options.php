<? /** @global CMain $APPLICATION */
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'mangotelecom.calltracking';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);
if ($moduleAccessLevel >= 'R') {
    Loader::includeModule($module_id);
    Loc::loadMessages($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/options.php');
    Loc::loadMessages(__FILE__);

    $arAllOptions = [
        [
            'SHOW_WIDGET',
            GetMessage('OPTIONS_MANGO_DCT_SHOW_WIDGET'),
            'N',
            ['checkbox']
        ],
        [
            'WIDGET_ID',
            GetMessage('OPTIONS_MANGO_DCT_WIDGET_ID'),
            '',
            ['text']
        ],
        [
            'PHONENUMBER_ELEM_SELECTOR',
            GetMessage('OPTIONS_MANGO_DCT_SELECTOR'),
            '.js-phone-number',
            [
                'text'
            ]
        ],
    ];

    $aTabs = [
        [
            'DIV' => 'edit0',
            'TAB' => 'Основные настройки',
            'TITLE' => 'Основные настройки модуля'
        ]
    ];
    $tabControl = new CAdminTabControl('mtalkerTabControl', $aTabs, true, true);


    if($REQUEST_METHOD == 'POST' && strlen($Update.$Apply.$RestoreDefaults) > 0 && check_bitrix_sessid()){

        foreach($arAllOptions as $arOption){

            $optionName = $arOption[0];
            $optionValue = $_REQUEST[$optionName];
            $fieldType = $arOption[3][0];

            if($fieldType == 'checkbox' && $optionValue != 'Y')
                $optionValue = 'N';


            Option::set($module_id, $optionName, $optionValue);

        }

        if(strlen($Update) > 0 && strlen($_REQUEST['back_url_settings']) > 0){
            LocalRedirect($_REQUEST['back_url_settings']);
        } else {
            LocalRedirect($APPLICATION->GetCurPage().'?mid='.urlencode($mid).'&lang='.urlencode(LANGUAGE_ID).'&back_url_settings='.urlencode($_REQUEST['back_url_settings']).'&'.$tabControl->ActiveTabParam());
        }
    }


    $tabControl->Begin();
    ?>
    <form method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<? echo LANGUAGE_ID ?>&mid=<?=$module_id?>" name="call_stat_settings">

        <?
        $tabControl->BeginNextTab();

        foreach($arAllOptions as $arOption){

            $optionName = $arOption[0];
            $optionTitle = $arOption[1];
            $optionDefaultValue = $arOption[2];
            $optionValue = Option::get($module_id, $optionName, $optionDefaultValue);
            $fieldType = $arOption[3][0];
            $fieldSizeAttr1 = $arOption[3][1];
            $fieldSizeAttr2 = $arOption[3][2];
            ?>
            <tr>
                <td width="40%" nowrap<? if($fieldType == 'textarea') echo ' class="adm-detail-valign-top"'; ?>>
                    <label for="<?=$optionName?>"><?=$optionTitle?>:</label>
                </td>
                <td width="60%">
                    <?
                    switch($fieldType){
                        case 'checkbox':
                            ?>
                            <input type="hidden" name="<?=$optionName?>" value="N">
                            <input type="checkbox" id="<?=$optionName?>" name="<?=$optionName?>"
                                   value="Y"<? if($optionValue == 'Y') echo ' checked'; ?>>
                        <? break;
                        case 'text':
                            ?>
                            <input type="text" size="<?=$fieldSizeAttr1?>" maxlength="255" value="<?=htmlspecialchars($optionValue)?>"
                                   name="<?=$optionName?>">
                        <? break;
                        case 'textarea':
                            ?>
                            <textarea rows="<?=$fieldSizeAttr1?>" cols="<?=$fieldSizeAttr2?>"
                                      name="<?=$optionName?>"><?=$optionValue?></textarea>
                        <? break;
                    } ?>
                </td>
            </tr>
        <? }

        $tabControl->Buttons();
        ?>
        <input type="submit" name="Update" value="<?=GetMessage('MAIN_SAVE')?>"
               title="<?=GetMessage('MAIN_OPT_SAVE_TITLE')?>" class="adm-btn-save"<? if ($moduleAccessLevel < 'W') echo ' disabled' ?>>
        <input type="submit" name="Apply" value="<?=GetMessage('MAIN_OPT_APPLY')?>"
               title="<?=GetMessage('MAIN_OPT_APPLY_TITLE')?>"<? if ($moduleAccessLevel < 'W') echo ' disabled' ?>>
        <? if(strlen($_REQUEST['back_url_settings']) > 0){ ?>
            <input type="button" name="Cancel" value="<?=GetMessage('MAIN_OPT_CANCEL')?>"
                   title="<?=GetMessage('MAIN_OPT_CANCEL_TITLE')?>"
                   onclick="window.location='<?=CUtil::addslashes($_REQUEST['back_url_settings'])?>'">
            <input type="hidden" name="back_url_settings" value="<?=$_REQUEST['back_url_settings']?>">
        <? } ?>
        <input type="submit" name="RestoreDefaults" title="<?=GetMessage('MAIN_HINT_RESTORE_DEFAULTS')?>"
               OnClick="return confirm('<?=AddSlashes(GetMessage('MAIN_HINT_RESTORE_DEFAULTS_WARNING'))?>')"
               value="<?=GetMessage('MAIN_RESTORE_DEFAULTS')?>">
        <input type="hidden" name="Update" value="Y">
        <?=bitrix_sessid_post();?>
    </form>
    <? $tabControl->End();
}