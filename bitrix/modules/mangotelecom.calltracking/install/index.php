<?

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class mangotelecom_calltracking extends CModule
{
    public $MODULE_ID = 'mangotelecom.calltracking';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public $errors;

    public function __construct() {
        $this->errors = false;

        $arModuleVersion = array();

        include(dirname(__FILE__) . '/version.php');

        $this->MODULE_ID = 'mangotelecom.calltracking';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('MDCT_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MDCT_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('MDCT_PARTNER_NAME');
        $this->PARTNER_URI = "https://www.mango-office.ru";
    }


    public function DoInstall() {
        global $USER;

        if ($USER->IsAdmin()) {
            $this->InstallFiles();
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallAgents();
        }
    }


    public function DoUninstall() {
        global $USER;

        if ($USER->IsAdmin()) {
            $this->UnInstallAgents();
            $this->UnInstallEvents();
            $this->UnInstallDB();
            $this->UnInstallFiles();
        }
    }


    function InstallDB() {
        RegisterModule($this->MODULE_ID);
        RegisterModuleDependences('main', 'OnEndBufferContent', $this->MODULE_ID, '\\Mango\\DCTWidget', 'onBeforeBufferEnd');
        $defaultOptions = Option::getDefaults($this->MODULE_ID);
        foreach ($defaultOptions as $option => $value)
            Option::set($this->MODULE_ID, $option, $value);

        return parent::InstallDB();
    }


    function UnInstallDB() {

        Option::delete($this->MODULE_ID);
        UnRegisterModule($this->MODULE_ID);
        UnRegisterModuleDependences('main', 'OnEndBufferContent', $this->MODULE_ID, '\\Mango\\DCTWidget', 'onBeforeBufferEnd');

        parent::UnInstallDB();
    }


    function InstallAgents(){

    }


    function UnInstallAgents(){

    }

}