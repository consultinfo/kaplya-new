<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);

$MODULE_ID = 'zaiv.snowflakes';

Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js');
$RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);

if($RIGHT != "W"){
	echo('Access denied to settings of '.$MODULE_ID);
}else{
	IncludeModuleLangFile(__FILE__);
	IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
	
	$strWarning = "";

	$ALLOWED_URLS = Option::get($MODULE_ID, "allowed_urls");
	if($ALLOWED_URLS){
		$ALLOWED_URLS = \Bitrix\Main\Web\Json::decode($ALLOWED_URLS);
	}

	$arOptions = [
		"main" => [
			["note" =>			GetMessage($MODULE_ID . "_OLD_VERSION_ALERT")],
			["enable",			GetMessage($MODULE_ID . "_ENABLE"), ["checkbox"]],
			["enable_admin",	GetMessage($MODULE_ID . "_ENABLE_ADMIN"), ["checkbox"]],
			["add_jquery",		GetMessage($MODULE_ID . "_ADD_JQUERY"), ["checkbox"]],
			["color",			GetMessage($MODULE_ID . "_COLOR"), ["text",20]],
			["shadow",			GetMessage($MODULE_ID . "_SHADOW"), ["checkbox"]],
			['type',			GetMessage($MODULE_ID . "_TYPE"),["selectbox",['1'=>GetMessage($MODULE_ID . "_TYPE_1"),'2'=>GetMessage($MODULE_ID . "_TYPE_2"),'3'=>GetMessage($MODULE_ID . "_TYPE_3"),'4'=>GetMessage($MODULE_ID . "_TYPE_4"),'RAND'=>GetMessage($MODULE_ID . "_TYPE_5")]]],
			['vol',				GetMessage($MODULE_ID . "_VOL"),["selectbox",['100'=>GetMessage($MODULE_ID . "_VOL_1"),'200'=>GetMessage($MODULE_ID . "_VOL_2"),'300'=>GetMessage($MODULE_ID . "_VOL_3"),'400'=>GetMessage($MODULE_ID . "_VOL_4"),'500'=>GetMessage($MODULE_ID . "_VOL_5"),'600'=>GetMessage($MODULE_ID . "_VOL_6"),'700'=>GetMessage($MODULE_ID . "_VOL_7"),'800'=>GetMessage($MODULE_ID . "_VOL_8"),'900'=>GetMessage($MODULE_ID . "_VOL_9"),'1000'=>GetMessage($MODULE_ID . "_VOL_10")]]],
			['speed',			GetMessage($MODULE_ID . "_SPEED"),["selectbox",['15000'=>GetMessage($MODULE_ID . "_SPEED_1"),'5000'=>GetMessage($MODULE_ID . "_SPEED_2"),'1'=>GetMessage($MODULE_ID . "_SPEED_3")]]],
		]
	];
	
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
		array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
	);
	
	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	
	if($REQUEST_METHOD == "POST" && strlen($Update . $Apply) > 0 && $RIGHT == "W" && check_bitrix_sessid()){
		Option::set($MODULE_ID, 'enable', ($_REQUEST['enable']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'enable_admin', ($_REQUEST['enable_admin']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'add_jquery', ($_REQUEST['add_jquery']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'color', $_REQUEST['color']);
		Option::set($MODULE_ID, 'shadow', ($_REQUEST['shadow']=="Y")?"Y":"N");
		Option::set($MODULE_ID, 'type', $_REQUEST['type']);
		Option::set($MODULE_ID, 'vol', $_REQUEST['vol']);
		Option::set($MODULE_ID, 'speed', $_REQUEST['speed']);

		$tmp_allowed_urls = array();
		if(count($_REQUEST['allowed_urls'])){
			foreach($_REQUEST['allowed_urls'] as $url){
				if(trim($url)){
					$tmp_allowed_urls[] = $url;
				}
			}
		}

		Option::set($MODULE_ID, 'allowed_urls', \Bitrix\Main\Web\Json::encode($tmp_allowed_urls, $options = null));

		ob_start();
		require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
		ob_end_clean();
	
		if(strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
			LocalRedirect($_REQUEST["back_url_settings"]);
		else
			LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
	}?>
	
	<form method="post" action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&amp;lang=<?= LANG ?>">
		<?
		$tabControl->Begin();
		$tabControl->BeginNextTab();

		foreach($arOptions['main'] as $optionArray) {
			$val = COption::GetOptionString($MODULE_ID, $optionArray[0]);
			array_splice($optionArray, 2, 0, $val);
			__AdmSettingsDrawRow($MODULE_ID, $optionArray);
		}
		?>
		<tr>
			<td valign="top"><label><?=GetMessage($MODULE_ID . "_ALLOWED_URLS")?>:</label></td>
			<td>
				<div id="allowed-urls-container">
					<?if(count($ALLOWED_URLS)){
						foreach($ALLOWED_URLS as $url){?>
							<input name="allowed_urls[]" class="allowed-url" type="text" value="<?=$url?>">
						<?}
					}?>
					<input name="allowed_urls[]" class="allowed-url" type="text" value="">
				</div>
				<input type="button" value="<?=Loc::getMessage($MODULE_ID . "_ALLOWED_URLS_ADD_BUTTON")?>" id="allowed-urls-add-button">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<div class="adm-info-message-wrap" align="center" wfd-id="16">
					<div class="adm-info-message" wfd-id="17">
						<?=GetMessage($MODULE_ID . "_NOTE_1")?>
					</div>
				</div>
			</td>
		</tr>

		<? $tabControl->BeginNextTab(); ?>
		<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?>
		<? $tabControl->Buttons(); ?>
		<div align="left">
			<input type="hidden" name="Update" value="Y">
			<input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>" class="adm-btn-save"/>
		</div>
		<? $tabControl->End(); ?>
		<?= bitrix_sessid_post(); ?>
	</form>

	<style>
		input.allowed-url{
			width: 65%;
		}
	</style>
	<script>
		$(function(){
			$('#allowed-urls-add-button').click(function(){
				$('#allowed-urls-container input:first-child').clone()
					.val("") 
					.appendTo("#allowed-urls-container");
			});
		});
	</script>
<?}?>