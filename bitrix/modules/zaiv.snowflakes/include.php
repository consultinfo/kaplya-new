<?
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

class CZaivSnowflakes
{
	function __construct()
	{

	}
	
	function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
	{
	
	}

}

global $APPLICATION;
global $USER;
$MODULE_ID = 'zaiv.snowflakes';

$allowed_urls = \Bitrix\Main\Web\Json::decode(Option::get($MODULE_ID, "allowed_urls"));

if(
	Option::get($MODULE_ID, 'enable') == 'Y' &&
	!CSite::InDir('/bitrix/') &&
	(
		(
			count($allowed_urls) &&
			in_array($APPLICATION->GetCurPage(),$allowed_urls)
		) ||
		(
			!count($allowed_urls)
		)
	) && (
		(
			Option::get($MODULE_ID, 'enable_admin') == 'Y' &&
			$USER->IsAdmin()
		) ||
		(
			!$USER->IsAdmin()
		) 
	)
){
	if(Option::get($MODULE_ID, 'add_jquery') == "Y"){
		Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js');
	}
?><script>
$(function(){$('body').append('<style>#zaiv-snowflake{position:fixed;z-index:100;font-size:25px;top:-50px;color:<?=Option::get($MODULE_ID, 'color')?>}.zaiv-snowflake-shadow{text-shadow:0 0 5px rgba(150,150,150,1)}</style>');$('body').append('<div id="zaiv-snowflake" <?=(Option::get($MODULE_ID, 'shadow') == 'Y')?' class="zaiv-snowflake-shadow"':''?>>&#10052;</div>');var sfa=["&#10052;","&#10053;","&#10054;","&#9913;"],sfs=<?=Option::get($MODULE_ID, 'speed')?>,sft="<?=Option::get($MODULE_ID, 'type')?>",sfv=<?=Option::get($MODULE_ID, 'vol')?>;function ri(a,e){var n=a-.5+Math.random()*(e-a+1);return n=Math.round(n)}var t=setInterval(function(){var a=$(document).height(),e=Math.random()*$(document).width()-100,n=.5+Math.random(),o=10+20*Math.random(),t=a-80,s=e-100+200*Math.random(),r=10*a+Math.random()*sfs;"RAND"==sft?$("#zaiv-snowflake").html(sfa[ri(0,3)]):$("#zaiv-snowflake").html(sfa[parseInt(sft)-1]),$("#zaiv-snowflake").clone().appendTo("body").css({left:e,opacity:n,"font-size":o}).animate({top:t,left:s,opacity:.1},r,"linear",function(){$(this).remove()})},sfv);});
</script><?
}


/*
���� ������� �������� ���������������� ���, �� ������ ���� ����-�� ����������� ������ ���������...

sfa = snowFlakesArr
sfs = snowFlakesSpeed
sft = snowFlakesType
sfv = snowFlakesVol
ri = randomInteger
*//*
$(function(){
	$('body').append('<style>#zaiv-snowflake{position:fixed;z-index:100;font-size:25px;top:-50px;color:<?=Option::get($MODULE_ID, 'color')?>}.zaiv-snowflake-shadow{text-shadow:0 0 5px rgba(150,150,150,1)}</style>');
	$('body').append('<div id="zaiv-snowflake" <?=(Option::get($MODULE_ID, 'shadow') == 'Y')?' class="zaiv-snowflake-shadow"':''?>>&#10052;</div>');

var snowFlakesArr = ['&#10052;','&#10053;','&#10054;','&#9913;'];
var snowFlakesSpeed = <?=$arParams['SNOWFLAKES_SPEED']?>;
var snowFlakesType = "<?=$arParams['SNOWFLAKES_TYPE']?>";
var snowFlakesVol = <?=$arParams['SNOWFLAKES_VOL']?>;

function randomInteger(min, max) {
	var rand = min - 0.5 + Math.random() * (max - min + 1)
	rand = Math.round(rand);
	return rand;
}		

var t = setInterval(
	function(){
		var documentHeight  = $(document).height();
		var startPositionLeft  = Math.random() * $(document).width() - 100;
		var startOpacity = 0.5 + Math.random();
		var sizeSnowFlake = 10 + Math.random() * 20;
		var endPositionTop = documentHeight - 80;
		var endPositionLeft = startPositionLeft - 100 + Math.random() * 200;
		var durationFall = documentHeight * 10 + Math.random() * snowFlakesSpeed;
		if(snowFlakesType=="RAND"){
			$('#zaiv-snowflake').html(snowFlakesArr[randomInteger(0,3)]);
		}else{
			$('#zaiv-snowflake').html(snowFlakesArr[parseInt(snowFlakesType)-1]);
		}
		$('#zaiv-snowflake')
			.clone()
			.appendTo('body')
			.css({
				left: startPositionLeft,
				opacity: startOpacity,
				'font-size': sizeSnowFlake,
			})
			.animate({
				top: endPositionTop,
				left: endPositionLeft,
				opacity: 0.1
			},
			durationFall,
			'linear',
			function() {
				$(this).remove()
			}
		);
	}, snowFlakesVol);
});
<?/**/?>
