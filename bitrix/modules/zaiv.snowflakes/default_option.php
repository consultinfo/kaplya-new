<?php
	$zaiv_snowflakes_default_option = array(
		'enable' => 'N',
		'enable_admin' => 'Y',
		'add_jquery' => 'Y',
		'color' => '#89d2ff',
		'shadow' => 'N',
		'type' => 'RAND',
		'vol' => '600',
		'speed' => '5000',
	);
?>