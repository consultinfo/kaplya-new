<?
use \Bitrix\Main\Application;

IncludeModuleLangFile(__FILE__);
Class zaiv_snowflakes extends CModule
{
	const MODULE_ID = 'zaiv.snowflakes';
	var $MODULE_ID = 'zaiv.snowflakes'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("ZAIV_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ZAIV_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("ZAIV_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("ZAIV_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p.'/'.$item, $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/'.$item, $ReWrite = True, $Recursive = True);
				}
				closedir($dir);
			}
		}
		return true;
	}

	function UnInstallFiles()
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
						continue;

					$dir0 = opendir($p0);
					while (false !== $item0 = readdir($dir0))
					{
						if ($item0 == '..' || $item0 == '.')
							continue;
						DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
					}
					closedir($dir0);
				}
				closedir($dir);
			}
		}
		return true;
	}
	
	function setAdminMsg(){
		CAdminNotify::Add(Array(
			"MESSAGE" =>  GetMessage("ADMIN_NOTIFY"),
			"MODULE_ID" => $this->MODULE_ID,
			"ENABLE_CLOSE" => "Y"
		));
		CAdminNotify::Add(Array(
			"MESSAGE" => GetMessage("ADMIN_NOTIFY_2"),
			"MODULE_ID" => $this->MODULE_ID,
			"ENABLE_CLOSE" => "Y"
		));
	}
	
	function delAdminMsg(){
		CAdminNotify::DeleteByModule($this->MODULE_ID);
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
		RegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID);
		$this->setAdminMsg();
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModuleDependences("main", "OnEpilog", $this->MODULE_ID);
		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
		$this->delAdminMsg();
	}
}
?>
