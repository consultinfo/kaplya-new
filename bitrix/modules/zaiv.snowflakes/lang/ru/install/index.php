<?
$MESS["ZAIV_MODULE_NAME"] = "Снежинки на вашем сайте";
$MESS["ZAIV_MODULE_DESC"] = "Простой модуль с минимумом настроек, добавляющий новогоднее настроение на ваш сайт :)";
$MESS["ZAIV_PARTNER_NAME"] = "ZaiV.RU - web development";
$MESS["ZAIV_PARTNER_URI"] = "http://zaiv.ru/marketplace/";
$MESS["ADMIN_NOTIFY"] = "Для запуска снежинок на вашем сайте перейдите в <a href=\"/bitrix/admin/settings.php?mid=zaiv.snowflakes&lang=ru\">настройки модуля</a> и включите его!";
$MESS["ADMIN_NOTIFY_2"] = "Если у вас возникнут вопросы по установке модуля \"Снежинки на вашем сайте\" - <a target=\"_blank\" href=\"https://zaiv.ru/contacts/\">напишите нам</a>, помощь будет бесплатной :)";
?>