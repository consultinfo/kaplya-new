<?
$MODULE_ID = 'zaiv.snowflakes';
$MESS[$MODULE_ID . "_OLD_VERSION_ALERT"] 	= "<span class=\"required\">Если вы использовали модуль до 19 декабря 2019 - пожалуйста удалите компонент \"zaiv:snowflakes\" из шаблона и/или страниц вашего сайта - он более не актуален</span><br>Если не удалите - то ничего страшного не произойдёт, всё по прежнему будет работать :)";
$MESS[$MODULE_ID . "_ENABLE"] 				= "Включить снежинки на сайте";
$MESS[$MODULE_ID . "_ENABLE_ADMIN"] 		= "Показывать снежинки администраторам?";
$MESS[$MODULE_ID . "_ADD_JQUERY"] 			= "Подключить jQuery?";
$MESS[$MODULE_ID . "_COLOR"] 				= "Цвет снежинок (Например: #89d2ff)";
$MESS[$MODULE_ID . "_TYPE"]					= "Тип снежинок";
$MESS[$MODULE_ID . "_TYPE_1"] 				= "Тип 1";
$MESS[$MODULE_ID . "_TYPE_2"] 				= "Тип 2";
$MESS[$MODULE_ID . "_TYPE_3"] 				= "Тип 3";
$MESS[$MODULE_ID . "_TYPE_4"] 				= "Тип 4";
$MESS[$MODULE_ID . "_TYPE_5"] 				= "Вперемежку";
$MESS[$MODULE_ID . "_SHADOW"] 				= "Добавить снежинкам тень";

$MESS[$MODULE_ID . "_VOL"] 	= "Количество снежинок";
$MESS[$MODULE_ID . "_VOL_1"] = "Очень, очень, очень много!";
$MESS[$MODULE_ID . "_VOL_2"] = "Очень, очень много";
$MESS[$MODULE_ID . "_VOL_3"] = "Очень много";
$MESS[$MODULE_ID . "_VOL_4"] = "Много";
$MESS[$MODULE_ID . "_VOL_5"] = "Нормально";
$MESS[$MODULE_ID . "_VOL_6"] = "Оптимально";
$MESS[$MODULE_ID . "_VOL_7"] = "Мало";
$MESS[$MODULE_ID . "_VOL_8"] = "Очень мало!";
$MESS[$MODULE_ID . "_VOL_9"] = "Очень, очень мало";
$MESS[$MODULE_ID . "_VOL_10"] = "Очень, очень, очень мало!";

$MESS[$MODULE_ID . "_SPEED"] 	= "Скорость падения снежинок";
$MESS[$MODULE_ID . "_SPEED_1"] 	= "Медленно";
$MESS[$MODULE_ID . "_SPEED_2"] 	= "Оптимально";
$MESS[$MODULE_ID . "_SPEED_3"] 	= "Быстро";

$MESS[$MODULE_ID . "_ALLOWED_URLS"] 		= "Показывать только по этим URL";
$MESS[$MODULE_ID . "_ALLOWED_URLS_ADD_BUTTON"] 	= "Добавить URL";


$MESS[$MODULE_ID . "_NOTE_1"] 	= "Есть вопросы или пожелания? <a target=\"_blank\" href=\"https://zaiv.ru/contacts/\">Будем рады выслушать, ответить и реализовать</a><br>И помните, что <b>хороший отзыв - лучшая награда</b> за бесплатный модуль, а написать его <a target=\"_blank\" href=\"http://marketplace.1c-bitrix.ru/solutions/zaiv.snowflakes/#tab-rating-link\">вы можете прямо сейчас</a> ;)";
