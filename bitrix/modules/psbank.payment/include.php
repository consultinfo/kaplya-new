<?
include_once __DIR__ . '/psbanklib/lib.php';
class CPsbankPayment
{
	static $module_id = "psbank.payment";

	public static function getModuleId()
	{
		return self::$module_id;
	}

	public static function testMode()
	{
		return CSalePaySystemAction::GetParamValue("DEMO", 1);
	}

	public static function getMerchant()
	{
		return trim(CSalePaySystemAction::GetParamValue("MERCHANT"));
	}

	public static function getMerchName()
	{
		return trim(CSalePaySystemAction::GetParamValue("MERCH_NAME"));
	}

	public static function getKey()
	{
		return trim(CSalePaySystemAction::GetParamValue("KEY", ""));
	}

	public static function getTerminal()
	{
		return trim(CSalePaySystemAction::GetParamValue("TERMINAL"));
	}

	public static function getTransactionType()
	{
		return trim(CSalePaySystemAction::GetParamValue("TRTYPE", 1));
	}

	public static function getNotify()
	{
		return (bool) CSalePaySystemAction::GetParamValue("NOTIFY", 0);
	}

	public static function getLogging()
	{
		return (bool) CSalePaySystemAction::GetParamValue("LOGGING", 0);
	}

	public static function getHost()
	{
		$domain = $_SERVER['SERVER_NAME'];
		$domain = str_ireplace(array('http://', 'https://'), '', rtrim($domain, '/'));

		return self::getSchema() . $domain;
	}

	public static function insertTransaction($order_id, $amount, $email, $trtype, $nonce)
	{
		global $DB;
		$DB->Query("INSERT INTO psbank_payment (ORDER_ID, AMOUNT, ORG_AMOUNT, EMAIL, DATE, TRTYPE, NONCE)
					VALUES('" . $DB->ForSql($order_id) . "','" . $DB->ForSql($amount) . "','" . $DB->ForSql($amount) . "','" . $DB->ForSql($email) . "',NOW()," . intval($trtype) . ",'" . $DB->ForSql($nonce) . "')
					ON DUPLICATE KEY UPDATE AMOUNT=VALUES(AMOUNT),ORG_AMOUNT=VALUES(ORG_AMOUNT),EMAIL=VALUES(EMAIL),DATE=NOW(),TRTYPE=VALUES(TRTYPE),NONCE=VALUES(NONCE)", false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	public static function updateTransaction($data)
	{
		global $DB;
		$fields = array("AUTHCODE", "CARD", "EMAIL", "INT_REF", "NAME", "RC", "RCTEXT", "RESULT", "RRN", "TRTYPE", "NONCE");

		if ($data['RESULT'] == 0) {
			$query[] = "STATUS='" . $DB->ForSql($data['TRTYPE']) . "'";
			if ($data['TRTYPE'] == 22 || $data['TRTYPE'] == 14) {
				$query[] = "AMOUNT=AMOUNT-'" . $DB->ForSql($data['AMOUNT']) . "'";
			} else {
				$query[] = "AMOUNT='" . $DB->ForSql($data['AMOUNT']) . "'";
			}
		}

		foreach ($fields as $field) {
			if (isset($data[$field]) && ($data[$field] || $data[$field] === '0')) {
				$query[] = "{$field}='" . $DB->ForSql($data[$field]) . "'";
			}
		}

		$DB->Query("UPDATE psbank_payment SET
			" . implode(',', $query) . ",
			DATE=NOW()
			WHERE ORDER_ID=" . intval($data['ORDER'])
		);

	}

	public static function insertHistory($data)
	{
		global $DB;
		if (!$data['ORG_AMOUNT']) {
			$data['ORG_AMOUNT'] = '0.00';
		}
		$DB->Query("INSERT INTO psbank_payment_history 	(AMOUNT, ORG_AMOUNT, CURRENCY, `ORDER`, `DESC`, MERCH_NAME, MERCHANT, TERMINAL, EMAIL,TRTYPE,`TIMESTAMP`,NONCE,RESULT, RC, RCTEXT, AUTHCODE, RRN, INT_REF, NAME, CARD, CHANNEL) VALUES ('" . $DB->ForSql($data['AMOUNT']) . "','" . $DB->ForSql($data['ORG_AMOUNT']) . "','" . $DB->ForSql($data['CURRENCY']) . "','" . $DB->ForSql($data['ORDER']) . "','" . $DB->ForSql($data['DESC']) . "','" . $DB->ForSql($data['MERCH_NAME']) . "','" . $DB->ForSql($data['MERCHANT']) . "','" . $DB->ForSql($data['TERMINAL']) . "','" . $DB->ForSql($data['EMAIL']) . "','" . $DB->ForSql($data['TRTYPE']) . "','" . $DB->ForSql($data['TIMESTAMP']) . "','" . $DB->ForSql($data['NONCE']) . "','" . $DB->ForSql($data['RESULT']) . "','" . $DB->ForSql($data['RC']) . "','" . $DB->ForSql($data['RCTEXT']) . "','" . $DB->ForSql($data['AUTHCODE']) . "','" . $DB->ForSql($data['RRN']) . "','" . $DB->ForSql($data['INT_REF']) . "','" . $DB->ForSql($data['NAME']) . "','" . $DB->ForSql($data['CARD']) . "','" . $DB->ForSql($data['CHANNEL']) . "')", false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}

	public static function preparePrice($sum)
	{
		if (class_exists("Bitrix\Sale\PriceMaths")) {
			return number_format(Bitrix\Sale\PriceMaths::roundByFormatCurrency($sum, Bitrix\Sale\Internals\SiteCurrencyTable::getSiteCurrency(SITE_ID)), 2, ".", "");
		} else {
			return number_format(round($sum, 2), 2, ".", "");
		}
	}

	public static function checkHTTPS()
	{
		if (!empty($_SERVER['HTTPS'])) {
			if ($_SERVER['HTTPS'] !== 'off') {
				return true;
			}
			//https
			else {
				return false;
			}
			//http
		} elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
			return true;
		} else
		if ($_SERVER['SERVER_PORT'] == 443) {
			return true;
		}
		//https
		else {
			return false;
		}
		//http
	}

	public static function getSchema()
	{
		return (self::checkHTTPS()) ? 'https://' : 'http://';
	}

	public static function getTransactionList($filter)
	{
		global $DB;
		$where = array();
		foreach ($filter as $key => $item) {
			$where[] = "$key = '" . $DB->ForSql($item) . "'";
		}
		if ($where) {
			$where = ' WHERE ' . implode(" AND ", $where);
		} else {
			$where = '';
		}
		$sql = "SELECT t.*,o.ACCOUNT_NUMBER FROM psbank_payment as t
		JOIN b_sale_order as o on o.ID=t.ORDER_ID
		$where
		order by ORDER_ID desc";
		$dbRes = $DB->Query($sql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		return $dbRes;
	}

	public static function getHistoryList($order_id)
	{
		global $DB;
		$sql   = "SELECT * FROM psbank_payment_history WHERE `ORDER`=" . intval($order_id) . " order by ID desc";
		$dbRes = $DB->Query($sql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		return $dbRes;
	}

	public static function getStatusName($status_id)
	{
		$status_id = intval($status_id);
		return GetMessage('PSBANK.PAYMENT_STATUS_' . $status_id);
	}

	public static function sendRequest($post)
	{
		global $DB;
		$res = false;
		foreach ($post['action'] as $order_id => $action) {
			$sum     = $post['sum'][$order_id];
			$arOrder = CSaleOrder::GetByID($order_id);
			CSalePaySystemAction::InitParamArrays($arOrder, $arOrder["ID"]);
			$date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
			$date->modify('-3Hours');
			$timestamp = $date->format('YmdHis');
			$sql       = "SELECT ORG_AMOUNT,RRN,INT_REF,EMAIL FROM psbank_payment WHERE ORDER_ID=" . intval($order_id);
			$dbRes     = $DB->Query($sql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			$data      = $dbRes->Fetch();
			$nonce     = sha1(mt_rand() . microtime());
			$params    = array(
				"ORDER"      => sprintf("%06s", $order_id),
				"AMOUNT"     => $sum,
				"CURRENCY"   => "RUB",
				"ORG_AMOUNT" => $data['ORG_AMOUNT'],
				"RRN"        => $data['RRN'],
				"INT_REF"    => $data['INT_REF'],
				"TRTYPE"     => $action,
				"TERMINAL"   => self::getTerminal(),
				"BACKREF"    => self::getHost() . '/bitrix/tools/psbank_backref.php?id=' . $order_id,
				"EMAIL"      => $data['EMAIL'],
				"TIMESTAMP"  => $timestamp,
				"NONCE"      => $nonce,
			);
			$params['P_SIGN'] = PSBankLib::calcHash($params, CPsbankPayment::getKey());
			if (CPsbankPayment::getNotify()) {
				$params['CARDHOLDER_NOTIFY'] = 'EMAIL';
			}
			if (CSalePaySystemAction::GetParamValue("MERCHANT_EMAIL", "")) {
				$params['MERCHANT_NOTIFY']       = 'EMAIL';
				$params['MERCHANT_NOTIFY_EMAIL'] = CSalePaySystemAction::GetParamValue("MERCHANT_EMAIL", "");
			}
			if(self::getLogging()) {
				PSBankLib::log(__DIR__.'/log.log', $params, 'actionrequest');
			}
			PSBankLib::request($params, CPsbankPayment::testMode());
			$res = array('order_id' => $order_id, 'action' => $action, 'nonce' => $nonce);
		}
		return $res;
	}

}
?>
