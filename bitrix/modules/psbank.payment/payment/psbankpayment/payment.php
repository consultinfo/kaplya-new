<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

IncludeModuleLangFile(__FILE__);
if (!CModule::IncludeModule("psbank.payment")) {
	return;
}

$order_id     = CSalePaySystemAction::GetParamValue("ORDER_ID");
$order_number = CSalePaySystemAction::GetParamValue("ORDER_NUMBER", $order_id);
$amount       = CPsbankPayment::preparePrice(CSalePaySystemAction::GetParamValue("SHOULD_PAY"));
$arOrder      = CSaleOrder::GetByID($order_id);

$url = PSBankLib::getUrl(CPsbankPayment::testMode());

$date = new DateTime('now', new DateTimeZone('Europe/Moscow'));
$date->modify('-3Hours');
$timestamp = $date->format('YmdHis');
$nonce     = sha1(mt_rand() . microtime());

$return_url = CPsbankPayment::getHost() . '/bitrix/tools/psbank_backref.php?id=' . $order_id . '&rnd=' . $nonce;
$merch_name = CPsbankPayment::getMerchName();

$data = array(
	'AMOUNT'     => $amount,
	'CURRENCY'   => 'RUB',
	'ORDER'      => sprintf("%06s", $order_id),
	'MERCH_NAME' => (ToUpper(SITE_CHARSET) != "UTF-8") ? iconv('cp1251', 'utf-8', $merch_name) : $merch_name,
	'MERCHANT'   => CPsbankPayment::getMerchant(),
	'TERMINAL'   => CPsbankPayment::getTerminal(),
	'EMAIL'      => CSalePaySystemAction::GetParamValue("EMAIL"),
	'TRTYPE'     => CPsbankPayment::getTransactionType(),
	'TIMESTAMP'  => $timestamp,
	'NONCE'      => $nonce,
	'BACKREF'    => $return_url,
);

$desc = GetMessage('PSBANK.PAYMENT_ORDER_DESC', array('#ORDER_ID#' => $order_number));

$sign               = PSBankLib::calcHash($data, CPsbankPayment::getKey());
$data['MERCH_NAME'] = $merch_name;
if (CPsbankPayment::getNotify()) {
	$data['CARDHOLDER_NOTIFY'] = 'EMAIL';
}
if (CSalePaySystemAction::GetParamValue("MERCHANT_EMAIL", "")) {
	$data['MERCHANT_NOTIFY']       = 'EMAIL';
	$data['MERCHANT_NOTIFY_EMAIL'] = CSalePaySystemAction::GetParamValue("MERCHANT_EMAIL", "");
}

if (CPsbankPayment::getLogging()) {
	PSBankLib::log(__DIR__ . '/../../log.log', $data, 'paymentform');
}

CPsbankPayment::insertTransaction($order_id, $data['AMOUNT'], $data['EMAIL'], 0, $data['NONCE']);
$button .= "<form method='POST' action='" . $url . "' accept-charset='UTF-8'>";
foreach ($data as $k => $v) {
	$button .= "<input type='hidden' name='" . $k . "' value='" . $v . "'>";
}
$button .= "<input type='hidden' name='P_SIGN' value='" . $sign . "'>
			<input type='hidden' name='DESC' value='" . $desc . "'>
			<input class='button btn btn-default psbank_payment_button' type='submit' value='" . GetMessage('PSBANK.PAYMENT_BUTTON_TEXT') . "' />
			</form>";
echo $button;
