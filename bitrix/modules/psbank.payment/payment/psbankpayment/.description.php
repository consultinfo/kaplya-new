<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("psbank.payment");
$psTitle = GetMessage("PSBANK.PAYMENT_TITLE");
$psDescription = GetMessage("PSBANK.PAYMENT_DESCRIPTION",array("#HOST#"=>CPsbankPayment::getHost()));

$arPSCorrespondence = array(
		"SHOULD_PAY" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_SHOULD_PAY"),
				"DESCR" => "",
				"VALUE" => "SHOULD_PAY",
				"TYPE" => "ORDER",
				"SORT" => -8,
				'DEFAULT' => array(
					'PROVIDER_KEY' => 'PAYMENT',
					'PROVIDER_VALUE' => 'SUM'
				)
			),
		"ORDER_ID" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_ORDER_ID"),
				"DESCR" => "",
				"VALUE" => "ID",
				"TYPE" => "ORDER",
				"SORT" => -7,
				'DEFAULT' => array(
					'PROVIDER_KEY' => 'ORDER',
					'PROVIDER_VALUE' => 'ID'
				)
			),
		"ORDER_NUMBER" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_ORDER_NUMBER"),
				"DESCR" => "",
				"VALUE" => "ID",
				"TYPE" => "ORDER",
				"SORT" => -6,
				'DEFAULT' => array(
					'PROVIDER_KEY' => 'ORDER',
					'PROVIDER_VALUE' => 'ACCOUNT_NUMBER'
				)
			),
		"EMAIL" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_EMAIL"),
				"DESCR" => "",
				"VALUE" => "EMAIL",
				"TYPE" => "PROPERTY",
				"SORT" => -5,
				'DEFAULT' => array(
					'PROVIDER_KEY' => 'USER',
					'PROVIDER_VALUE' => 'EMAIL'
				)
			),
		"TERMINAL" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_TERMINAL"),
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => "",
				"SORT" => -4
			),
		"MERCHANT" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_MERCHANT"),
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => "",
				"SORT" => -3
			),
		"KEY" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_KEY"),
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => "",
				"SORT" => -2
			),
		"MERCH_NAME" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_MERCH_NAME"),
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => "",
				"SORT" => -1
			),
		"DEMO" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_DEMO"),
				"DESCR" => "",
				"VALUE" => array(
					"1"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_DEMO_YES_OPTION")),
					"0"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_DEMO_NO_OPTION")),
					),
				"TYPE" => "SELECT"
			),
		"LOGGING" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_LOGGING"),
				"DESCR" => "",
				"VALUE" => array(
					"1"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_LOGGING_YES_OPTION")),
					"0"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_LOGGING_NO_OPTION")),
					),
				"TYPE" => "SELECT"
			),
		"TRTYPE" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_TRTYPE"),
				"DESCR" => "",
				"VALUE" => array(
					"1"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_TRTYPE_1_OPTION")),
					"12"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_TRTYPE_12_OPTION")),
					),
				"TYPE" => "SELECT"
			),
		"NOTIFY" => array(
				"NAME" => GetMessage("PSBANK.PAYMENT_NOTIFY"),
				"DESCR" => "",
				"VALUE" => array(
					"0"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_NOTIFY_NO")),
					"1"=>array('NAME' =>GetMessage("PSBANK.PAYMENT_NOTIFY_YES")),
					),
				"TYPE" => "SELECT",
				'DEFAULT' => "0"
			),
		"MERCHANT_EMAIL" => array(
				"NAME" => GetMessage("PSBANK.MERCHANT_EMAIL"),
				"DESCR" => "",
				"VALUE" => "",
				"TYPE" => "",
				'DEFAULT' => "0"
			),
	);
?>