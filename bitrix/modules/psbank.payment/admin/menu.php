<?php

IncludeModuleLangFile(__FILE__);

if($APPLICATION->GetGroupRight("psbank.payment")!="D")
{
	$aMenu = array(
		"parent_menu" => "global_menu_store",
		"section" => "psbank_payment",
		"sort" => 100,
		"text" => GetMessage("PSBANK.PAYMENT_MENU_TITLE"),
		"title" => GetMessage("PSBANK.PAYMENT_MENU_TITLE"),
		"url" => "psbank_payment.php?lang=".LANGUAGE_ID,
		"icon" => "psbank_menu_icon",
		"page_icon" => "psbank_menu_icon",
		"items_id" => "menu_psbank_payment",
		"more_url" => array(
				"psbank_payment_history.php"
			),
	);
	return $aMenu;
}
return false;
