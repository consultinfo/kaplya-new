<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/general/admin_tool.php");

IncludeModuleLangFile(__FILE__);

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if($saleModulePermissions == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
ClearVars("l_");

CModule::IncludeModule("psbank.payment");
CModule::IncludeModule("sale");


$arUserGroups = $USER->GetUserGroupArray();
$intUserID = intval($USER->GetID());

$sTableID = "psbank_payment";

$oSort = new CAdminSorting($sTableID, "ORDER_ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arPost = array();
$action_success = false;
if(is_array($_POST) && count($_POST)) {
    $arPost = $_POST;
    $resdata = CPsbankPayment::sendRequest($arPost);
    $exit = 0;
    $counter = 20;
    do {
        sleep(1);
        $dbRes = $DB->Query('SELECT TRTYPE, RESULT, RCTEXT FROM psbank_payment WHERE ORDER_ID="'.$DB->ForSql($resdata['order_id']).'" AND NONCE="'.$DB->ForSql($resdata['nonce']).'"');
        $data = $dbRes->Fetch();
        if ($resdata && $data && $data['TRTYPE'] == $resdata['action']) {
            $exit = 1;
            if($data['RESULT'] == 0 ){
                $action_success = true;
            }
        }
        $counter--;
    } while(!$exit && $counter);
    if(!$action_success){
        if ($exit) {
            $lAdmin->AddUpdateError(GetMessage('PSBANK.PAYMENT_FAIL',array('#ERROR#' => $data['RCTEXT'])));
        } else {
            $lAdmin->AddUpdateError(GetMessage('PSBANK.PAYMENT_NOTIFY_FAIL'));
        }
    }

}






$arFilterFields = array(
    "filter_order_id",
    "filter_rrn",
);

$lAdmin->InitFilter($arFilterFields);
$arFilter = array();
if(trim($filter_order_id)!='') $arFilter["ACCOUNT_NUMBER"] = trim($filter_order_id);
if($filter_rrn!='') $arFilter["RRN"] = trim($filter_rrn);

$arFilterFieldsTmp = array(
    "filter_order_id" => GetMessage("PSBANK.PAYMENT_TRANSACTION_ORDER_NUMBER"),
    "filter_rrn" => 'RRN',
);

$oFilter = new CAdminFilter(
    $sTableID."_filter",
    $arFilterFieldsTmp
);
/*
$oFilter->SetDefaultRows(array("filter_universal"));

$oFilter->AddPreset(array(
        "ID" => "find_prioritet",
        "NAME" => GetMessage("SOA_PRESET_PRIORITET"),
        "FIELDS" => array(
            "filter_status" => "N",
            "filter_price_from" => "10000",
            "filter_price_to" => ""
            ),
    ));
*/
$arHeaders = array(
    array("id"=>"ORDER_ID","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_NUMBER'), "sort"=>false, "default"=>true),
    array("id"=>"AMOUNT","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_SUM'), "sort"=>false, "default"=>true),
    array("id"=>"DATE","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_DATE'), "sort"=>false, "default"=>true),
    array("id"=>"EMAIL","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_EMAIL'), "sort"=>false, "default"=>true),
    array("id"=>"NAME","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_NAME'), "sort"=>false, "default"=>true),
    array("id"=>"STATUS","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_STATUS'), "sort"=>false, "default"=>true),
    array("id"=>"RRN","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_RRN'), "sort"=>false, "default"=>true),
    array("id"=>"ACTION","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ACTION'), "sort"=>false, "default"=>true),
    array("id"=>"HISTORY","content"=>'', "sort"=>false, "default"=>true),
);

$lAdmin->AddHeaders($arHeaders);


    //array("nPageSize"=>CAdminResult::GetNavSize($sTableID))

$dbOrderList = CPsbankPayment::getTransactionList($arFilter);



$dbOrderList = new CAdminResult($dbOrderList, $sTableID);
$dbOrderList->NavStart();

$lAdmin->NavText($dbOrderList->GetNavPrint(""));

while ($arOrder = $dbOrderList->NavNext(true, "f_"))
{
    $row =& $lAdmin->AddRow($f_ID, $arOrder, "sale_order_view.php?ID=".$arOrder['ORDER_ID']."&lang=".LANGUAGE_ID.GetFilterParams("filter_"));

    $idTmp = '<a href="/bitrix/admin/sale_order_view.php?ID='.$arOrder["ORDER_ID"].'" title="'.GetMessage("PSBANK.PAYMENT_VIEW_ORDER").'">'.$arOrder["ACCOUNT_NUMBER"].'</a>';

    $row->AddField("ORDER_ID", $idTmp);
    /*if ($_POST && isset($_POST['action']) && $action_success) {
        $action = reset($_POST['action']);
        $order_id = key($_POST['action']);
        if ($arOrder["ORDER_ID"] == $order_id) {
            $arOrder["STATUS"] = $action;
            $arOrder["RCTEXT"] = '';
            if (in_array($action, array(22, 14))) {
                $arOrder["AMOUNT"] -= $_POST['sum'][$order_id];
            } else {
                $arOrder["AMOUNT"] = $_POST['sum'][$order_id];
            }
        }
    }*/
    $status = CPsbankPayment::getStatusName($arOrder["STATUS"]);
    if($arOrder["RCTEXT"]){
        $status .= " ({$arOrder["RCTEXT"]})";
    }
    $row->AddField("STATUS", $status);
    $action = '';
    if(in_array($arOrder["STATUS"], array(1, 21, 12, 22, 14)) && $arOrder["AMOUNT"]!=0) {
    $action = '
            <input type="text" name="sum['.$arOrder['ORDER_ID'].']" value="'.$arOrder["AMOUNT"].'">
            <br>';

        if(in_array($arOrder["STATUS"], array(1, 21, 14))) {
            $action .= '
                <button class="adm-btn" type="submit" name="action['.$arOrder['ORDER_ID'].']" value="14">'.GetMessage('PSBANK.PAYMENT_ACTION_RETURN').'</button>';
        }
         if(in_array($arOrder["STATUS"],array(12, 22))) {
            $action .= '
                <button class="adm-btn" type="submit" name="action['.$arOrder['ORDER_ID'].']" value="22">'.GetMessage('PSBANK.PAYMENT_ACTION_CANCEL').'</button>';
            $action .= '
                <button class="adm-btn" type="submit" name="action['.$arOrder['ORDER_ID'].']" value="21">'.GetMessage('PSBANK.PAYMENT_ACTION_CONFIRM').'</button>';
        }
    }
    $row->AddField('ACTION', $action);
    $history_link = '<a href="/bitrix/admin/psbank_payment_history.php?ID='.$arOrder['ORDER_ID'].'">'.GetMessage('PSBANK.PAYMENT_HISTORY_LINK').'</a>';
    $row->AddField('HISTORY', $history_link);

}

$lAdmin->CheckListMode();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");

$APPLICATION->SetTitle(GetMessage("PSBANK.PAYMENT_TRANSACTION_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?
$oFilter->Begin();
?>
<tr>
    <td><?=GetMessage("PSBANK.PAYMENT_TRANSACTION_ORDER_NUMBER")?>:</td>
    <td>
        <input type="text" name="filter_order_id" value="<?echo htmlspecialcharsbx($filter_order_id)?>" size="40">
    </td>
</tr>
<tr>
    <td>RRN:</td>
    <td>
        <input type="text" name="filter_rrn" value="<?echo htmlspecialcharsbx($filter_rrn)?>" size="40">
    </td>
</tr>
<?
$oFilter->Buttons(
    array(
        "table_id" => $sTableID,
        "url" => $APPLICATION->GetCurPage(),
        "form" => "find_form"
    )
);
$oFilter->End();
?>
</form>
<?
$lAdmin->DisplayList();
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>