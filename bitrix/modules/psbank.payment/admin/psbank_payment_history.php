<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/general/admin_tool.php");

IncludeModuleLangFile(__FILE__);

$saleModulePermissions = $APPLICATION->GetGroupRight("sale");
if($saleModulePermissions == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
ClearVars("l_");

CModule::IncludeModule("psbank.payment");
CModule::IncludeModule("sale");


IncludeModuleLangFile(__FILE__);

$arUserGroups = $USER->GetUserGroupArray();
$intUserID = intval($USER->GetID());

$sTableID = "psbank_payment_history";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);


$arFilter = Array();


$arHeaders = array(
    array("id"=>"TIMESTAMP","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_TIMESTAMP'), "sort"=>false, "default"=>true),
    array("id"=>"AMOUNT","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_AMOUNT'), "sort"=>false, "default"=>true),
    array("id"=>"ORG_AMOUNT","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_ORG_AMOUNT'), "sort"=>false, "default"=>true),
    array("id"=>"TRTYPE","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_TRTYPE'), "sort"=>false, "default"=>true),
    array("id"=>"RESULT","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_RESULT'), "sort"=>false, "default"=>true),
    array("id"=>"RC","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_RC'), "sort"=>false, "default"=>true),
    array("id"=>"AUTHCODE","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_AUTHCODE'), "sort"=>false, "default"=>true),
    array("id"=>"RRN","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_RRN'), "sort"=>false, "default"=>true),
    array("id"=>"INT_REF","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_INT_REF'), "sort"=>false, "default"=>true),
    array("id"=>"NAME","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_NAME'), "sort"=>false, "default"=>true),
    array("id"=>"CARD","content"=>GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_CARD'), "sort"=>false, "default"=>true),

);

$lAdmin->AddHeaders($arHeaders);

$dbOrderList = CPsbankPayment::getHistoryList($_GET['ID']);



$dbOrderList = new CAdminResult($dbOrderList, $sTableID);
$dbOrderList->NavStart();

$lAdmin->NavText($dbOrderList->GetNavPrint(""));

while ($arOrder = $dbOrderList->NavNext(true, "f_"))
{
    $row =& $lAdmin->AddRow($f_ID, $arOrder, "sale_order_detail.php?ID=".$arOrder['ORDER']."&lang=".LANGUAGE_ID.GetFilterParams("filter_"));
    $row->AddField('RC', $arOrder['RC'].' '.$arOrder['RCTEXT']);
    $ts = $arOrder['TIMESTAMP'];
    $row->AddField('TIMESTAMP', "{$ts[0]}{$ts[1]}{$ts[2]}{$ts[3]}-{$ts[4]}{$ts[5]}-{$ts[6]}{$ts[7]} {$ts[8]}{$ts[9]}:{$ts[10]}{$ts[11]}:{$ts[12]}{$ts[13]}");
    $row->AddField('TRTYPE', GetMessage('PSBANK.PAYMENT_TRANSACTION_ORDER_TRTYPE_'.$arOrder["TRTYPE"]));
}

$lAdmin->CheckListMode();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/sale/prolog.php");
$arOrder = CSaleOrder::GetByID($_GET['ID']);
$APPLICATION->SetTitle(GetMessage("PSBANK.PAYMENT_HISTORY_TITLE", array('#ORDER_ID#' => $arOrder['ACCOUNT_NUMBER'])));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<?
$lAdmin->DisplayList();
?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>