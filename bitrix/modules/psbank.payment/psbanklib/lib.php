<?php

class PSBankLib
{
	static $version = "1.1.0";

	public static function calcHash($data, $key)
	{
		$params = array(
			'PAYMENT'        => array(
				"AMOUNT", "CURRENCY", "ORDER", "MERCH_NAME", "MERCHANT", "TERMINAL", "EMAIL", "TRTYPE", "TIMESTAMP", "NONCE", "BACKREF",
			),
			'PAYMENT_NOTIFY' => array(
				"AMOUNT", "CURRENCY", "ORDER", "MERCH_NAME", "MERCHANT", "TERMINAL", "EMAIL", "TRTYPE", "TIMESTAMP", "NONCE", "BACKREF", "RESULT", "RC", "RCTEXT", "AUTHCODE", "RRN", "INT_REF",
			),
			'ACTION'         => array(
				"ORDER", "AMOUNT", "CURRENCY", "ORG_AMOUNT", "RRN", "INT_REF", "TRTYPE", "TERMINAL", "BACKREF", "EMAIL", "TIMESTAMP", "NONCE",
			),
			'ACTION_NOTIFY'  => array(
				"ORDER", "AMOUNT", "CURRENCY", "ORG_AMOUNT", "RRN", "INT_REF", "TRTYPE", "TERMINAL", "BACKREF", "EMAIL", "TIMESTAMP", "NONCE", "RESULT", "RC", "RCTEXT",
			),
		);
		$sign = '';
		if (in_array($data['TRTYPE'], array(1, 12))) {
			$type = 'PAYMENT';
		} else {
			$type = 'ACTION';
		}
		if (isset($data['RESULT'])) {
			$type .= '_NOTIFY';
		}
		foreach ($params[$type] as $param) {
			$value = $data[$param];
			if ($value == "") {
				$sign .= "-";
			} else {
				$sign .= mb_strlen($value, "8bit") . $value;
			}
		}
		$hash = hash_hmac("sha1", $sign, pack("H*", $key));
		return strtoupper($hash);
	}

	public static function getUrl($test_mode)
	{
		if ($test_mode) {
			$url = 'https://test.3ds.payment.ru/cgi-bin/cgi_link';
		} else {
			$url = 'https://3ds.payment.ru/cgi-bin/cgi_link';
		}
		return $url;
	}

	public static function request($data, $test_mode)
	{
		$url = self::getUrl($test_mode);
		$ch  = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_USERAGENT, 'PSBankLib ' . self::$version);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	public static function log($filePath, $message, $category , $maxFileSizeMb = 10, $maxLogFiles = 10, $fileMode = 0664, $dirMode = 0775 ) {
		$maxFileSize = $maxFileSizeMb / $maxLogFiles * 1024 * 1024;
		$dir = dirname($filePath);

		if (!is_dir($dir)) {
			@mkdir($dir, $dirMode);
			chmod($dir, $dirMode);
		}

		$needToRotate = @filesize($filePath) > $maxFileSize;
		if ($needToRotate) {
			$file = $filePath;

			for ($i = $maxLogFiles; $i >= 0; --$i) {
				$rotateFile = $file . ($i === 0 ? '' : '.' . $i);

				if (is_file($rotateFile)) {
					if ($i === $maxLogFiles) {
						@unlink($rotateFile);
					} else {
						@rename($rotateFile, $file . '.' . ($i + 1));
					}
				}
			}
		}

		if (!file_exists($filePath)) {
			@touch($filePath);
			chmod($filePath, $fileMode);
		}

		if (!is_scalar($message) ) {
			$message = var_export($message, true);
		}

		$line = sprintf('%s [%s]: %s', date('Y-m-d H:i:s'), $category, $message);
		$line .= "\n\n\n";

		file_put_contents($filePath, $line, FILE_APPEND);
	}
}
