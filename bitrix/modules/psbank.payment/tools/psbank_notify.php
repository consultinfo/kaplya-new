<?php
error_reporting(E_ERROR | E_PARSE);

define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);

if($_GET["admin_section"]=="Y")
	define("ADMIN_SECTION", true);
else
	define("BX_PUBLIC_TOOLS", true);
if(!require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php")) die('prolog_before.php not found!');
IncludeModuleLangFile(__FILE__);
if(CModule::IncludeModule("psbank.payment") && CModule::IncludeModule("sale")) {
	$order_id 		= intval($_POST['ORDER']);
	$arOrder = CSaleOrder::GetByID($order_id);

	if(!$arOrder)return;
	$paymentObject = null;
	if(class_exists('\Bitrix\Sale\Order') && class_exists('\Bitrix\Sale\Internals\PaymentTable')) {
		$order = \Bitrix\Sale\Order::load($order_id);
		$paymentCollection = $order->getPaymentCollection();
		$payment = null;
		foreach($paymentCollection as $p){
			if (
				$p->getPaySystem()->getField('ACTION_FILE') == '/bitrix/php_interface/include/sale_payment/psbankpayment'
			) {
				if ( (
						($_POST['ORG_AMOUNT'] == '' && CPsbankPayment::preparePrice($p->getField('SUM')) == $_POST['AMOUNT'])
						|| ($_POST['ORG_AMOUNT'] != '' && CPsbankPayment::preparePrice($p->getField('SUM')) == $_POST['ORG_AMOUNT'])
					)
					&& (
						$p->getField('PAID') == 'N'
						|| count($paymentCollection) == 1
					)
				) {
					$paymentObject = $p;
					$payment = $p;
					break;
				} else {
					$payment = $p;
					$paymentObject = $p;
				}
			}
		}
		$payment = \Bitrix\Sale\Internals\PaymentTable::getRow(
						array(
							'select' => array('*'),
							'filter' => array('ORDER_ID' => $order_id, 'PAY_SYSTEM_ID' => $payment->getPaySystem()->getField('PAY_SYSTEM_ID'))
						)
					);
		CSalePaySystemAction::InitParamArrays($arOrder, $arOrder["ID"], "", array(), $payment);
	} else {
		CSalePaySystemAction::InitParamArrays($arOrder, $arOrder["ID"]);
	}

	$sign = PSBankLib::calcHash($_POST, CPsbankPayment::getKey());

	if(strcmp($sign, $_POST['P_SIGN']) === 0){
		CPsbankPayment::updateTransaction($_POST);
		CPsbankPayment::insertHistory($_POST);
		if(($_POST['TRTYPE'] == 1 || $_POST['TRTYPE'] == 21) && $_POST['RESULT'] == 0) {
			$arFields = array(
					"PS_STATUS"				=> 'Y',
					"PS_STATUS_CODE"		=> $_POST['RC'],
					"PS_STATUS_DESCRIPTION"	=> $_POST['RCTEXT'],
					"PS_STATUS_MESSAGE"		=> $_POST['NAME'].' '.$_POST['CARD'],
					"PS_RESPONSE_DATE"		=> new \Bitrix\Main\Type\DateTime(),
					"PS_SUM"				=> $_POST['AMOUNT']
				);
			if($paymentObject) {
				$paymentObject->setFields($arFields);
				$paymentObject->setPaid('Y');
				$order->save();
			} else {
				$arFields["PS_RESPONSE_DATE"] = Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG)),strtotime($_POST['TIMESTAMP']));
				if ($arOrder["PAYED"] != "Y" && Doubleval($arOrder["PRICE"]) == DoubleVal($arFields["PS_SUM"]))
				{
					CSaleOrder::PayOrder($arOrder["ID"], "Y", true, true);
				}

				CSaleOrder::Update($arOrder["ID"], $arFields);
			}
		}
	}
}
