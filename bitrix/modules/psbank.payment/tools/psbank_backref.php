<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule('sale');


$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle(GetMessage("PSBANK.BACKREF_TITLE"));

$orderID = $_REQUEST['id'];
$res = $DB->Query("SELECT `ORDER` as ORDER_ID, TRTYPE, RESULT,RCTEXT FROM psbank_payment_history WHERE `ORDER`=".intval($orderID).' and NONCE="'.$DB->ForSql($_REQUEST['rnd']).'" ORDER BY `TIMESTAMP` DESC ');
$data = $res->Fetch();
if (!$data) {
		$res = $DB->Query("SELECT ORDER_ID, TRTYPE, RESULT,RCTEXT FROM psbank_payment WHERE ORDER_ID=".intval($orderID).' and NONCE="'.$DB->ForSql($_REQUEST['rnd']).'"');
	$data = $res->Fetch();
}
$order = false;
if ($data) {
	$order = CSaleOrder::GetByID($data['ORDER_ID']);
}

if($order){
	$statusPageURL = sprintf('%s?ID=%s', GetPagePath('personal/order'), (int)$orderID);
}

?>

<?php if (!$order): ?>
	<?php echo GetMessage("PSBANK.BACKREF_NOTFOUND", array('#ORDER_ID#' => htmlspecialchars($orderID) ));?>
<?php else: ?>
	<?php if(($data['TRTYPE'] == 1 || $data['TRTYPE'] == 12) && ($data['RESULT'] == 0 || $data['RCTEXT'] == 'Approved')):?>
	<?php echo GetMessage("PSBANK.BACKREF_THANK");?>
	<?php else:
		$msg = $data['RCTEXT']?GetMessage("PSBANK.BACKREF_ERROR").' '.$data['RCTEXT']:GetMessage("PSBANK.BACKREF_WAITING");
		?>
		<?php echo htmlspecialchars($msg)?>
	<?php endif;?>
	<br/>
	<?php echo GetMessage("PSBANK.BACKREF_ORDERLINK", array('#LINK#'=>$statusPageURL));?>
<?php endif; ?>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>