<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
include(GetLangFileName($strPath2Lang."/lang/", "/install.php"));


Class psbank_payment extends CModule
{
    var $MODULE_ID = "psbank.payment";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";
    var $PARTNER_NAME;
    var $PARTNER_URI;


    function psbank_payment()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'psbank.payment';
        $this->MODULE_NAME = GetMessage('PSBANK.PAYMENT_INSTALL_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('PSBANK.PAYMENT_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('PSBANK.PAYMENT_PARTNER_NAME');
        $this->PARTNER_URI = 'https://www.psbank.ru/';
    }

    function DoInstall()
    {
        $this->InstallFiles();
        $this->InstallDB();
        $GLOBALS["errors"] = $this->errors;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $GLOBALS["errors"] = $this->errors;
    }

    function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/sale_payment/psbankpayment/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/sale_payment/psbankpayment/");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/themes/.default", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/themes/icons", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/icons", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin/psbank_payment.php", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/psbank_payment.php");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/admin/psbank_payment_history.php", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/psbank_payment_history.php");
        CheckDirPath($_SERVER["DOCUMENT_ROOT"]."bitrix/images/sale/sale_payments/bitrix/php_interface/include/sale_payment");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/images/psbankpayment.png", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/sale/sale_payments/bitrix/php_interface/include/sale_payment/psbankpayment.png");
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/admin/psbank_payment.php");
        DeleteDirFilesEx("/bitrix/admin/psbank_payment_history.php");
        DeleteDirFilesEx("/bitrix/php_interface/include/sale_payment/psbankpayment");
        DeleteDirFilesEx("/bitrix/images/sale/sale_payments/bitrix/php_interface/include/sale_payment/psbankpayment.png");
        DeleteDirFilesEx("/bitrix/admin/psbank_payment_history.php");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools/");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$this->MODULE_ID."/install/themes/.default", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default");
        DeleteDirFilesEx( $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default/icons/psbank.payment");
        return true;
    }


    function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;
        RegisterModule("psbank.payment");
        if(!$DB->Query("SELECT 'x' FROM psbank_payment WHERE 1=0", true)){
            $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/psbank.payment/install/db/".$DBType."/install.sql");
        }
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/psbank.payment/install/db/".$DBType."/uninstall.sql");
        UnRegisterModule("psbank.payment");
        return true;
    }

}
?>