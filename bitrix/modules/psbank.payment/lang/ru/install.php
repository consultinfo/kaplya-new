<?
global $MESS;
$MESS ['PSBANK.PAYMENT_PARTNER_NAME'] = "ПАО «Промсвязьбанк»";
$MESS ['PSBANK.PAYMENT_INSTALL_NAME'] = "Промсвязьбанк: интернет-эквайринг";
$MESS ['PSBANK.PAYMENT_INSTALL_DESCRIPTION'] = "Приём оплаты по карте через интернет-эквайринг ПАО «Промсвязьбанк»";
$MESS ['PSBANK.PAYMENT_INSTALL_TITLE'] = "Установка модуля платёжных сервисов";
$MESS ['PSBANK.PAYMENT_INSTALL_PUBLIC_DIR'] = "Публичная папка";
$MESS ['PSBANK.PAYMENT_INSTALL_SETUP'] = "Установить";
$MESS ['PSBANK.PAYMENT_INSTALL_COMPLETE_OK'] = "Установка завершена. Для дополнительной помощи обратитесь в раздел помощи.";
$MESS ['PSBANK.PAYMENT_INSTALL_COMPLETE_ERROR'] = "Установка завершена с ошибками";
$MESS ['PSBANK.PAYMENT_INSTALL_ERROR'] = "Ошибки при установке";
$MESS ['PSBANK.PAYMENT_INSTALL_BACK'] = "Вернуться в управление модулями";
$MESS ['PSBANK.PAYMENT_UNINSTALL_WARNING'] = "Внимание! Модуль будет удален из системы.";
$MESS ['PSBANK.PAYMENT_UNINSTALL_SAVEDATA'] = "Вы можете сохранить данные в таблицах базы данных, если установите флажок &quot;Сохранить таблицы&quot;";
$MESS ['PSBANK.PAYMENT_UNINSTALL_SAVETABLE'] = "Сохранить таблицы";
$MESS ['PSBANK.PAYMENT_UNINSTALL_DEL'] = "Удалить";
$MESS ['PSBANK.PAYMENT_UNINSTALL_ERROR'] = "Ошибки при удалении:";
$MESS ['PSBANK.PAYMENT_UNINSTALL_COMPLETE'] = "Удаление завершено.";
$MESS ['PSBANK.PAYMENT_INSTALL_PUBLIC_SETUP'] = "Установить";
$MESS ['PSBANK.PAYMENT_INSTALL_UNPOSSIBLE'] = "Деинсталляция модуля невозможна.";
?>