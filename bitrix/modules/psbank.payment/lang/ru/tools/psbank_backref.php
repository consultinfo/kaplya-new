<?php
$MESS['PSBANK.BACKREF_NOTFOUND'] = 'Заказ с кодом #ORDER_ID# не найден';
$MESS['PSBANK.BACKREF_THANK'] = 'Спасибо за оплату заказа';
$MESS['PSBANK.BACKREF_ERROR'] = 'Ошибка при оплате заказа:';
$MESS['PSBANK.BACKREF_ORDERLINK'] = 'Состояние заказа можно узнать на <a href="#LINK#">странице заказа</a>';
$MESS['PSBANK.BACKREF_TITLE'] = "Оплата заказа";
$MESS['PSBANK.BACKREF_WAITING'] = "Ожидание информации об оплате";
