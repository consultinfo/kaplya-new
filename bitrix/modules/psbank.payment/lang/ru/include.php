<?php
global $MESS;
$MESS['PSBANK.PAYMENT_STATUS_0'] = 'Ожидается оплата';
$MESS['PSBANK.PAYMENT_STATUS_1'] = 'Заказ оплачен';
$MESS['PSBANK.PAYMENT_STATUS_12'] = 'Заказ предавторизован';
$MESS['PSBANK.PAYMENT_STATUS_21'] = 'Заказ оплачен';
$MESS['PSBANK.PAYMENT_STATUS_22'] = 'Предавторизация отменена';
$MESS['PSBANK.PAYMENT_STATUS_14'] = 'Возврат оплаты';