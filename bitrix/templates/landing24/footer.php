<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>

<?php
if (\Bitrix\Main\Loader::includeModule('landing'))
{
	\Bitrix\Landing\Manager::setTheme();
}
?>

<?$APPLICATION->ShowProperty('FooterJS');?>


<script>
	BX.ready(function() {
		var elements = [].slice.call(document.querySelectorAll("h1, h2, h3, h4, h5"));
		new BX.Landing.UI.Tool.autoFontScale(elements);
	});
</script>
</main>
<?$APPLICATION->ShowProperty('BeforeBodyClose');?>
<!-- CLEANTALK template addon -->
<?php \Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area"); if(CModule::IncludeModule("cleantalk.antispam")) echo CleantalkAntispam::FormAddon(); \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area", "Loading..."); ?>
<!-- /CLEANTALK template addon -->
</body>
</html>