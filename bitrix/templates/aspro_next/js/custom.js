/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
jQuery(document).ready(function () {
    function show_popup(){
        jQuery('.layer').fadeIn(200);
        jQuery(".discount-message-wrap").fadeIn(200);
    };
    window.setTimeout( show_popup, 15000 ); // 15 seconds
   jQuery('.discount-message__close').on('click', function () {
       jQuery('.layer').fadeOut(200);
       jQuery('.discount-message-wrap').fadeOut(200);
   });
});