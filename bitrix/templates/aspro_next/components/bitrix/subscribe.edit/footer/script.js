$(document).ready(function () {
    const subscribeForm = $("form.subscribe-form");

    subscribeForm.validate
    ({
            rules: {
                "EMAIL": {
                    required: true,
                    email: true
                }
            },
            messages: {
                "captcha_word": {
                    remote: '<?=GetMessage("VALIDATOR_CAPTCHA")?>'
                },
            },
        }
    );

    subscribeForm.on('submit', (ev) => {
        if (!subscribeForm.valid()) {
            ev.preventDefault()
        }

        if (grecaptcha.getResponse().length === 0) {
            $('.g-recaptcha > div').css('border', '1px solid red');
            ev.preventDefault()
        } else {
            $('.g-recaptcha > div').css('border', 'none');
        }
    })
})