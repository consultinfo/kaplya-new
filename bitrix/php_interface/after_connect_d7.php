<?php
/* Ansible managed: /etc/ansible/roles/web/templates/after_connect_d7.php.j2 modified on 2015-06-15 11:31:17 by root on vmi27602 */
$connection = Bitrix\Main\Application::getConnection();
$connection->queryExecute("SET sql_mode=''"); 
$connection->queryExecute("SET NAMES 'utf8'");
$connection->queryExecute("SET collation_connection = 'utf8_unicode_ci'");
$connection->queryExecute("SET innodb_strict_mode=0");
#timezone
$connection->queryExecute("SET LOCAL time_zone='".date('P')."'");
