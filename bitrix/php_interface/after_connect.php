<?php
/* Ansible managed: /etc/ansible/roles/web/templates/after_connect.php.j2 modified on 2015-06-15 11:31:17 by root on vmi27602 */
$DB->Query("SET NAMES 'utf8'");
$DB->Query('SET collation_connection = "utf8_unicode_ci"');
$DB->Query("SET sql_mode=''");

#timezone
$DB->Query("SET LOCAL time_zone='".date('P')."'");