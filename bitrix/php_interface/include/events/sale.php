<?php
$eventManager = \Bitrix\Main\EventManager::getInstance();
/**
 * В почтовое событие о новом заказе добавляем следующие поля:
 *
 * Номер телефона                 - $arFields["PHONE"]
 *
 * @param int $orderId Id заказа
 * @param $eventName
 * @param $arFields
 */

$eventManager->AddEventHandler("sale", "OnOrderNewSendEmail", "OnOrderNewSendEmailHandler");
function OnOrderNewSendEmailHandler($orderId, &$eventName, &$arFields)
{
    $db_props = CSaleOrderPropsValue::GetOrderProps($arFields["ORDER_ID"]);

    while($arProps = $db_props->Fetch()) {
        if ($arProps["CODE"] == "FIO") {
            $arFields["FIO"] = $arProps["VALUE"];
        }
        if ($arProps["CODE"] == "PHONE") {
            $arFields["PHONE"] = $arProps["VALUE"];
        }
    }

}